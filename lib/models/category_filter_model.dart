class CategoryFilterModel {
  CategoryFilterModel({
    this.categories,
  });

  List<Category>? categories;

  factory CategoryFilterModel.fromJson(Map<String, dynamic> json) =>
      CategoryFilterModel(
        categories: List<Category>.from(
            json["categories"].map((x) => Category.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "categories": List<dynamic>.from(categories!.map((x) => x.toJson())),
      };
}

class Category {
  Category({
    required this.id,
    required this.title,
    required this.subs,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  String title;
  List<SubCategory> subs;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        id: json["id"],
        title: json["title"],
        subs: List<SubCategory>.from(
            json["subs"].map((x) => SubCategory.fromJson(x))),
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "subs": subs,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
      };

  @override
  String toString() {
    return '{"id": "${id}", "title": "${title}", "subs": "${subs.toString()}", "created_at": "${createdAt}", "updated_at": "${updatedAt}"}';
  }
}

class SubCategory {
  SubCategory({
    this.id,
    required this.title,
    required this.category_id,
    this.createdAt,
    this.updatedAt,
  });

  dynamic id;
  String title;
  int category_id;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory SubCategory.fromJson(Map<String, dynamic> json) => SubCategory(
        id: json["id"],
        title: json["title"],
        category_id: json["category_id"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "category_id": category_id,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
      };

  @override
  String toString() {
    return '{"id": "${id}", "title": "${title}", "category_id": "${category_id}", "created_at": "${createdAt}", "updated_at": "${updatedAt}"}';
  }
}
