class PaymentDetail {
  PaymentDetail({
    this.key,
    this.pass,
    this.razorkey,
    this.razorpass,
    this.paytmkey,
    this.paytmpass,
  });

  String? key;
  String? pass;
  String? razorkey;
  String? razorpass;
  String? paytmkey;
  String? paytmpass;

  factory PaymentDetail.fromJson(Map<String, dynamic> json) => PaymentDetail(
        key: json["key"],
        pass: json["pass"],
        razorkey: json["razorkey"],
        razorpass: json["razorpass"],
        paytmkey: json["paytmkey"],
        paytmpass: json["paytmpass"],
      );

  Map<String, dynamic> toJson() => {
        "key": key,
        "pass": pass,
        "razorkey": razorkey,
        "razorpass": razorpass,
        "paytmkey": paytmkey,
        "paytmpass": paytmpass,
      };
}
