class PaymentKeyModel {
  String? key;
  String? pass;
  String? razorkey;
  String? razorpass;
  String? paytmkey;
  String? paytmpass;
  String? imapikey;
  String? imauthtoken;
  String? imurl;
  String? paypalClientId;
  String? paypalSecretId;
  String? paypalMode;

  PaymentKeyModel({
    this.key,
    this.pass,
    this.razorkey,
    this.razorpass,
    this.paytmkey,
    this.paytmpass,
    this.imapikey,
    this.imauthtoken,
    this.imurl,
    this.paypalClientId,
    this.paypalSecretId,
    this.paypalMode,
  });

  PaymentKeyModel.fromJson(Map<String, dynamic> json) {
    key = json['key'];
    pass = json['pass'];
    razorkey = json['razorkey'];
    razorpass = json['razorpass'];
    paytmkey = json['paytmkey'];
    paytmpass = json['paytmpass'];
    imapikey = json['imapikey'];
    imauthtoken = json['imauthtoken'];
    imurl = json['imurl'];
    paypalClientId = json['paypalClientId'];
    paypalSecretId = json['paypalSecretId'];
    paypalMode = json['paypalMode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['key'] = this.key;
    data['pass'] = this.pass;
    data['razorkey'] = this.razorkey;
    data['razorpass'] = this.razorpass;
    data['paytmkey'] = this.paytmkey;
    data['paytmpass'] = this.paytmpass;
    data['imapikey'] = this.imapikey;
    data['imauthtoken'] = this.imauthtoken;
    data['imurl'] = this.imurl;
    data['paypalClientId'] = this.paypalClientId;
    data['paypalSecretId'] = this.paypalSecretId;
    data['paypalMode'] = this.paypalMode;
    return data;
  }
}
