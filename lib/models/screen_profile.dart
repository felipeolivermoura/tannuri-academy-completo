class ScreenProfile {
  int id;
  String? screenName;
  String? screenStatus;

  ScreenProfile(this.id, this.screenName, this.screenStatus);
}
