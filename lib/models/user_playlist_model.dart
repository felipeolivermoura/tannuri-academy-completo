import 'wishlist_model.dart';

class UserPlaylistModel {
  UserPlaylistModel({this.playlists});

  List<UserPlaylist>? playlists;

  factory UserPlaylistModel.fromJson(Map<String, dynamic> json) =>
      UserPlaylistModel(
        playlists: List<UserPlaylist>.from(
            json["playlists"].map((x) => UserPlaylist.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "playlists": List<dynamic>.from(playlists!.map((x) => x.toJson())),
      };
}

class UserPlaylist {
  UserPlaylist({
    required this.id,
    required this.name,
    required this.order,
    required this.wishlist,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  String name;
  int order;
  List<Wishlist>? wishlist;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory UserPlaylist.fromJson(Map<String, dynamic> json) => UserPlaylist(
        id: json["id"],
        name: json["name"],
        order: json["p_order"],
        wishlist: List<Wishlist>.from(
            json["wishlist"].map((x) => Wishlist.fromJson(x))),
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "order": order,
        "wishlist": wishlist,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
      };

  @override
  String toString() {
    return '{"id": "${id}", "name": "${name}", "order": "${order}", "wishlist": ${wishlist.toString()}, "created_at": "${createdAt}", "updated_at": "${updatedAt}"}';
  }
}
