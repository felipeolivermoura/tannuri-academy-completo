import 'datum.dart';

class MovieTv {
  MovieTv({
    this.data,
  });

  List<Datum>? data;

  factory MovieTv.fromJson(Map<String, dynamic> json) => MovieTv(
      data: List<Datum>.from(json["movie"].map((x) => Datum.fromJson(x))));

  Map<String, dynamic> toJson() =>
      {"movie": List<dynamic>.from(data!.map((x) => x.toJson()))};
}
