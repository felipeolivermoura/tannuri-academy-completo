import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:tannuri/common/apipath.dart';
import 'package:tannuri/common/global.dart';
import 'package:tannuri/common/route_paths.dart';

Future freeSub(BuildContext context, dynamic id, double amount,
    String reference, dynamic status, String free) async {
  final freeSubscription =
      await http.post(Uri.parse(APIData.freeSubscription), headers: {
    HttpHeaders.authorizationHeader: "Bearer $authToken",
    "Accept": "application/json",
  }, body: {
    "plan_id": '$id',
    "amount": '$amount',
    "reference": '$reference',
    "status": '$status',
    "method": '$free',
  });
  print('refrence  $reference');
  if (freeSubscription.statusCode == 200) {
    print('Free Subscription Status Code : ${freeSubscription.statusCode}');
    Fluttertoast.showToast(
      msg: translate("Subscribed_Successfully"),
    );
    Navigator.pushNamed(context, RoutePaths.splashScreen);
  } else {
    print('Free Subscription Status Code : ${freeSubscription.statusCode}');
    Fluttertoast.showToast(
      msg: translate("Error_in_subscription"),
    );
  }
  return null;
}
