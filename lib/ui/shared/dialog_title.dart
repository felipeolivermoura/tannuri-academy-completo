import 'package:flutter/material.dart';

class DialogTitle extends StatelessWidget {
  final String title;

  const DialogTitle({
    Key? key,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 4),
      child: Column(
        children: [
          Text(
            "$title",
            style: TextStyle(fontSize: 18),
            textAlign: TextAlign.center,
          ),
          Container(
            margin: EdgeInsets.only(top: 8),
            decoration: BoxDecoration(
              border: Border(
                bottom:
                    BorderSide(width: 0.5, color: Colors.grey.withOpacity(0.3)),
              ),
            ),
            child: SizedBox(
              width: double.maxFinite,
              height: 2,
            ),
          ),
        ],
      ),
    );
  }
}
