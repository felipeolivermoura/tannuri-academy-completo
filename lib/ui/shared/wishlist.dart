import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:provider/provider.dart';
import 'package:tannuri/providers/user_playlist_provider.dart';
import 'package:tannuri/ui/shared/dialog_title.dart';
import '/common/apipath.dart';
import '/common/global.dart';
import '/models/datum.dart';
import 'package:http/http.dart' as http;

class WishListView extends StatefulWidget {
  WishListView(this.videoDetail);

  final Datum? videoDetail;

  @override
  _WishListViewState createState() => _WishListViewState();
}

class _WishListViewState extends State<WishListView> {
  bool checkWishlist = false;
  bool _visible = false;
  late StateSetter _setState;

  TextEditingController playlistNameController = new TextEditingController();

  checkWishList(vType, id) async {
    var res;
    final response = await http.get(
        Uri.parse(
            "${APIData.checkWatchlistMovie}$id?secret=" + APIData.secretKey),
        headers: {HttpHeaders.authorizationHeader: "Bearer $authToken"});
    print(response.statusCode);
    print(response.body);
    if (response.statusCode == 200) {
      res = json.decode(response.body);
      if (res['wishlist'] == 1 || res['wishlist'] == "1") {
        setState(() {
          checkWishlist = true;
          _visible = true;
        });
      } else {
        setState(() {
          checkWishlist = false;
          _visible = true;
        });
      }
    } else {
      throw "Can't check wishlist";
    }
  }

  removeWishList(vType, id) async {
    final response = await http.get(
        Uri.parse(
            "${APIData.removeWatchlistMovie}$id?secret=" + APIData.secretKey),
        headers: {HttpHeaders.authorizationHeader: "Bearer $authToken"});
    if (response.statusCode == 200) {
      setState(() {
        checkWishlist = false;
      });

      await http.delete(Uri.parse("${APIData.userPlaylistsApi}/wishlist/$id"),
          headers: {HttpHeaders.authorizationHeader: "Bearer $authToken"});
    } else {
      throw "Can't remove from wishlist";
    }
  }

  addWishList(vType, id, int? playlistId) async {
    var type = "M";
    loadingDialog();
    final response = await http.post(Uri.parse("${APIData.addWatchlist}"),
        body: {"type": type, "id": '$id', "value": '1'},
        headers: {HttpHeaders.authorizationHeader: "Bearer $authToken"});
    print(response.statusCode);
    print(response.body);
    if (response.statusCode == 200) {
      final wishlist = jsonDecode(response.body);
      await http.post(Uri.parse("${APIData.userPlaylistsApi}/wishlist"), body: {
        "wishlist_id": "${wishlist['id']}",
        "playlist_id": "$playlistId"
      }, headers: {
        HttpHeaders.authorizationHeader: "Bearer $authToken"
      });

      setState(() {
        checkWishlist = true;
      });
      Navigator.of(context).pop();
      Navigator.of(context).pop();
    } else {
      throw "Can't added to wishlist";
    }
  }

  Widget setupPlaylistContainer(vType, id) {
    final userPlaylists =
        Provider.of<UserPlaylistProvider>(context, listen: false)
            .userPlaylistModel;
    print("userPlaylists ${userPlaylists?.playlists!.length}");

    return Container(
      width: double.maxFinite,
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: userPlaylists?.playlists!.length != 0
            ? userPlaylists?.playlists!.length
            : 1,
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            title: userPlaylists?.playlists!.length != 0
                ? Text("${userPlaylists?.playlists![index].name}")
                : Text(
                    "Nenhuma playlist encontrada",
                    textAlign: TextAlign.center,
                  ),
            onTap: () => userPlaylists?.playlists!.length != 0
                ? addWishList(vType, id, userPlaylists?.playlists![index].id)
                : null,
          );
        },
      ),
    );
  }

  Future showPlaylists(vType, id) => showDialog(
        context: context,
        builder: (context) =>
            StatefulBuilder(builder: (context, StateSetter setState) {
          _setState = setState;

          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            title: DialogTitle(
              title: "Selecione uma playlist",
            ),
            titlePadding: EdgeInsets.only(top: 6),
            content: setupPlaylistContainer(vType, id),
            actions: [
              TextButton(
                onPressed: addDialog,
                child: Text("CRIAR PLAYLIST"),
              ),
              TextButton(
                onPressed: addCancel,
                child: Text("CANCELAR"),
              )
            ],
          );
        }),
      );

  Future<void> addDialog() => showDialog<void>(
        context: context,
        builder: (context) => AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          title: DialogTitle(
            title: "Nova Playlist",
          ),
          titlePadding: EdgeInsets.only(top: 6),
          content: TextField(
            controller: playlistNameController,
            decoration: InputDecoration(
              hintText: "Nome da playlist",
            ),
          ),
          actions: [
            TextButton(
              onPressed: addCancel,
              child: Text("CANCELAR"),
            ),
            TextButton(
              onPressed: addSubmit,
              child: Text("SALVAR"),
            )
          ],
        ),
      );

  void addCancel() => Navigator.of(context).pop();
  void addSubmit() async => {
        if (playlistNameController.text != '' &&
            playlistNameController.text != 'null')
          {
            await loadPlaylists(),
            _setState(() {}),
            Navigator.of(context).pop(),
            playlistNameController.text = '',
          }
      };

  Future<String?> loadPlaylists() async {
    loadingDialog();
    String playlistName = playlistNameController.text;
    UserPlaylistProvider userPlaylistProvider =
        Provider.of<UserPlaylistProvider>(context, listen: false);

    var request = http.MultipartRequest(
      'POST',
      Uri.parse(APIData.userPlaylistsApi),
    );
    var headers = {
      "Content-Type": "application/x-www-form-urlencoded",
      HttpHeaders.authorizationHeader: "Bearer $authToken",
    };

    request.headers.addAll(headers);
    request.fields['name'] = playlistName;

    await request.send();

    await userPlaylistProvider.getUserPlaylist(context);
    Navigator.of(context).pop();

    return null;
  }

  loadingDialog() {
    return showDialog<void>(
      context: context,
      builder: (context) => AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        title: DialogTitle(
          title: "Aguarde...",
        ),
        titlePadding: EdgeInsets.only(top: 6),
        content: Container(
          width: 200,
          height: 150,
          child: Center(
            child: CircularProgressIndicator(
              color: Theme.of(context).primaryColor,
            ),
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      _visible = false;
      playlistNameController.text = '';
    });
    checkWishList("M", widget.videoDetail!.id);
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          child: _visible == false
              ? Center(
                  child: CircularProgressIndicator(
                    color: Theme.of(context).primaryColor,
                  ),
                )
              : Column(
                  children: [
                    checkWishlist == false
                        ? Icon(
                            Icons.add,
                            size: 30.0,
                          )
                        : Icon(
                            Icons.check,
                            color: activeDotColor,
                            size: 30.0,
                          ),
                    new Padding(
                      padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 10.0),
                    ),
                    checkWishlist == true
                        ? Text(
                            translate("Wishlist_"),
                            style: TextStyle(
                              fontFamily: 'Lato',
                              fontSize: 12.0,
                              fontWeight: FontWeight.w600,
                              letterSpacing: 0.0,
                              color: activeDotColor,
                            ),
                          )
                        : Text(
                            translate("Wishlist_"),
                            style: TextStyle(
                              fontFamily: 'Lato',
                              fontSize: 12.0,
                              fontWeight: FontWeight.w600,
                              letterSpacing: 0.0,
                            ),
                          ),
                  ],
                ),
          onTap: () {
            if (checkWishlist == true) {
              removeWishList("M", widget.videoDetail!.id);
            } else {
              showPlaylists("M", widget.videoDetail!.id);
              // addWishList(widget.videoDetail!.type, widget.videoDetail!.id);
            }
          },
        ),
      ),
    );
  }
}
