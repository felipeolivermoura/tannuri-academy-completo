import 'package:flutter/material.dart';
import 'package:tannuri/common/styles.dart';

class CheckBoxModel {
  CheckBoxModel({
    required this.id,
    required this.title,
    this.checked = false,
    required this.subs,
  });

  int id;
  String title;
  bool checked;
  List<CheckSubBoxModel> subs;

  @override
  String toString() {
    return '{"id": "${id}", "title": "$title", "checked": "$checked", "subs": "${subs.toString()}"}';
  }
}

class CheckSubBoxModel {
  CheckSubBoxModel(
      {required this.id, required this.title, this.checked = false});

  int id;
  String title;
  bool checked;

  @override
  String toString() {
    return '{"id": "${id}", "title": "$title", "checked": "$checked"}';
  }
}

class CheckboxWidget extends StatefulWidget {
  const CheckboxWidget({Key? key, required this.item}) : super(key: key);

  final CheckBoxModel item;

  @override
  _CheckboxWidgetState createState() => _CheckboxWidgetState();
}

class _CheckboxWidgetState extends State<CheckboxWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CheckboxListTile(
          title: Text(widget.item.title),
          value: widget.item.checked,
          selected: widget.item.checked,
          activeColor: primaryBlue,
          checkColor: kGrey900,
          onChanged: (value) {
            setState(() {
              widget.item.checked = value != null ? value : false;
              for (int index = 0; index < widget.item.subs.length; index += 1)
                widget.item.subs[index].checked = value != null ? value : false;
            });
          },
        ),
        for (int index = 0; index < widget.item.subs.length; index += 1)
          CheckboxListTile(
            title: Container(
              padding: EdgeInsets.only(left: 15),
              child: Text(
                widget.item.subs[index].title,
                style: TextStyle(fontSize: 14),
              ),
            ),
            value: widget.item.subs[index].checked,
            selected: widget.item.subs[index].checked,
            activeColor: kTeal400,
            checkColor: kGrey900,
            onChanged: (value) {
              setState(() {
                widget.item.subs[index].checked = value != null ? value : false;

                final check = widget.item.subs.where((s) => s.checked == true);
                if (value != null && value == false && check.isEmpty) {
                  widget.item.checked = false;
                } else
                  widget.item.checked = true;
              });
            },
          ),
      ],
    );
  }
}
