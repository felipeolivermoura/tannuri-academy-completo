import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:tannuri/ui/widgets/horizontal_plans_list.dart';
import '/providers/app_config.dart';
import '/providers/user_profile_provider.dart';
import 'package:provider/provider.dart';

class SubPlanScreen extends StatefulWidget {
  @override
  _SubPlanScreenState createState() => _SubPlanScreenState();
}

class _SubPlanScreenState extends State<SubPlanScreen> {
//  List used to show all the plans using home API

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      await getUSerData();
    });
  }

  Future getUSerData() async {
    var userDetailsProvider =
        Provider.of<UserProfileProvider>(context, listen: false);
    await userDetailsProvider.getUserProfile(context);
  }

// Scaffold body
  Widget scaffoldBody() {
    var planDetails = Provider.of<AppConfig>(context).planList;

    return planDetails.length == 0
        ? noPlanColumn()
        : Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
              child: HorizontalPlansList(),
            ),
          );
  }

  //  Empty watchlist container message
  Widget noPlanContainer() {
    return Padding(
      padding: EdgeInsets.only(left: 50.0, right: 50.0),
      child: Text(
        translate("No_subscription_plans_available"),
        style: TextStyle(
          height: 1.5,
        ),
        textAlign: TextAlign.center,
      ),
    );
  }

//  Empty watchlist icon
  Widget noPlanIcon() {
    return Image.asset(
      "assets/no_plan.png",
      height: 140,
      width: 160,
    );
  }

//  Empty plan column
  Widget noPlanColumn() {
    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          noPlanIcon(),
          SizedBox(
            height: 25.0,
          ),
          noPlanContainer(),
        ],
      ),
    );
  }

//  Build Method
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: false,
      child: ClipRRect(
        borderRadius: new BorderRadius.only(
          topLeft: Radius.circular(30),
          topRight: Radius.circular(30),
        ),
        child: Scaffold(
          appBar: AppBar(
            automaticallyImplyLeading: false,
            actions: [
              Container(
                margin: EdgeInsets.only(right: 20),
                child: InkWell(
                  child: Icon(
                    Icons.close,
                    size: 30,
                  ),
                  onTap: () => Navigator.of(context).pop(),
                ),
              ),
            ],
            backgroundColor: Colors.transparent,
          ),
          body: FutureBuilder(
            future: getUSerData(),
            builder: (context, AsyncSnapshot dataSnapshot) {
              if (dataSnapshot.connectionState == ConnectionState.waiting) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              } else {
                if (dataSnapshot.error != null) {
                  return Center(
                    child: Text(translate('An_error_occurred_')),
                  );
                } else {
                  return scaffoldBody();
                }
              }
            },
          ),
        ),
      ),
    );
  }
}
