import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:tannuri/providers/user_playlist_provider.dart';
import '../../providers/count_view_provider.dart';
import '/common/apipath.dart';
import '/common/global.dart';
import '/common/route_paths.dart';
import '/common/styles.dart';
import '/models/login_model.dart';
import '/providers/app_config.dart';
import '/providers/login_provider.dart';
import '/providers/movie_tv_provider.dart';
import '/providers/user_profile_provider.dart';
import '/ui/shared/logo.dart';
import '/ui/widgets/register_here.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController _emailController = new TextEditingController();
  final TextEditingController _passwordController = new TextEditingController();
  bool _isHidden = true;
  String msg = '';
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  bool _isLoading = false;
  bool isLoggedIn = false;
  var profileData;
  bool isShowing = false;
  late LoginModel loginModel;

  @override
  void initState() {
    super.initState();
  }

  updateScreens(screen, count, index) async {
    final updateScreensResponse =
        await http.post(Uri.parse(APIData.updateScreensApi), body: {
      "macaddress": '$ip',
      "screen": '$screen',
      "count": '$count',
    }, headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      HttpHeaders.authorizationHeader: "Bearer $authToken"
    });
    if (updateScreensResponse.statusCode == 200) {
      storage.write(
          key: "screenName", value: "${screenList[index].screenName}");
      storage.write(key: "screenStatus", value: "YES");
      storage.write(key: "screenCount", value: "${screenList[index].id + 1}");
      storage.write(
          key: "activeScreen", value: "${screenList[index].screenName}");
      Navigator.pushNamed(context, RoutePaths.bottomNavigationHome);
    } else {
      Fluttertoast.showToast(msg: translate("Error_in_selecting_profile"));
      throw "Can't select profile";
    }
  }

  bool get isLoading {
    return false;
  }

  void onLoginStatusChanged(bool isLoggedIn, {profileData}) {
    setState(() {
      this.isLoggedIn = isLoggedIn;
      this.profileData = profileData;
    });
  }

  Future<void> fetchAppData(ctx) async {
    UserProfileProvider userProfileProvider =
        Provider.of<UserProfileProvider>(ctx, listen: false);
    MovieTVProvider movieTVProvider =
        Provider.of<MovieTVProvider>(ctx, listen: false);
    await userProfileProvider.getUserProfile(ctx);
    await movieTVProvider.getMoviesTVData(ctx);
    await Provider.of<CountViewProvider>(ctx, listen: false).loadData();
    setState(() {
      isShowing = false;
    });
    setState(() {
      isShowing = false;
    });
    Navigator.pushNamed(context, RoutePaths.bottomNavigationHome);
  }

  Future<void> _saveForm() async {
    FocusScope.of(context).requestFocus(new FocusNode());
    final loginProvider = Provider.of<LoginProvider>(context, listen: false);
    final isValid = _formKey.currentState!.validate();
    if (!isValid) {
      return;
    }
    _formKey.currentState!.save();
    setState(() {
      _isLoading = true;
    });
    try {
      print("sss1:");
      await loginProvider.login(
          _emailController.text, _passwordController.text, context);
      print("sss2:");
      if (loginProvider.loginStatus == true) {
        print("sss3:");
        final userPlaylistProvider =
            Provider.of<UserPlaylistProvider>(context, listen: false);
        await userPlaylistProvider.getUserPlaylist(context);

        Navigator.pushNamed(context, RoutePaths.bottomNavigationHome);
      } else if (loginProvider.emailVerify == false) {
        print("sss4:");
        setState(() {
          setState(() {
            _isLoading = false;
            _emailController.text = '';
            _passwordController.text = '';
          });
        });
        showAlertDialog(context, loginProvider.emailVerifyMsg);
      } else {
        print("sss5:");
        setState(() {
          _isLoading = false;
        });
        Fluttertoast.showToast(
          msg: "The user credentials were incorrect..!",
          backgroundColor: Colors.red,
          textColor: Colors.white,
          gravity: ToastGravity.BOTTOM,
        );
      }
    } catch (error) {
      print("sss: $error");
      await showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          backgroundColor: Colors.white,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
          title: Text(
            'An error occurred!',
            style: TextStyle(
              color: Colors.black.withOpacity(0.7),
              fontWeight: FontWeight.bold,
            ),
          ),
          content: Text(
            'Something went wrong',
            style: TextStyle(
              color: Colors.black.withOpacity(0.6),
            ),
          ),
          actions: <Widget>[
            TextButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color?>(
                  Colors.blueAccent,
                ),
              ),
              child: Text('OK'),
              onPressed: () {
                Navigator.pop(context);
              },
            )
          ],
        ),
      );
    }
    setState(() {
      _isLoading = false;
    });
  }

  showAlertDialog(BuildContext context, String msg) {
    var msg1 = msg.replaceAll('"', "");
    Widget okButton = TextButton(
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all<Color?>(
          primaryRed,
        ),
      ),
      child: Text("OK"),
      onPressed: () {
        Navigator.pop(context);
      },
    );

    AlertDialog alert = AlertDialog(
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      title: Text(
        "Verify Email!",
        textAlign: TextAlign.center,
        style: TextStyle(
            color: primaryRed, fontSize: 22.0, fontWeight: FontWeight.bold),
      ),
      content: Text("$msg1 Verify email sent on your register email.",
          style: TextStyle(
            color: Theme.of(context).colorScheme.background,
            fontSize: 16.0,
          )),
      actions: [
        okButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  goToDialog() {
    if (isShowing == true) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) => WillPopScope(
              child: AlertDialog(
                backgroundColor: Colors.white,
                title: Row(
                  children: [
                    CircularProgressIndicator(
                      valueColor: new AlwaysStoppedAnimation<Color>(primaryRed),
                    ),
                    SizedBox(
                      width: 15.0,
                    ),
                    Text(
                      "Loading ..",
                      style: TextStyle(
                          color: Theme.of(context).colorScheme.background),
                    )
                  ],
                ),
              ),
              onWillPop: () async => false));
    } else {
      Navigator.pop(context);
    }
  }

  resetPasswordAlertBox() =>
      Navigator.pushNamed(context, RoutePaths.forgotPassword);

// Toggle for visibility
  void _toggleVisibility() {
    setState(() {
      _isHidden = !_isHidden;
    });
  }

  Widget emailField() {
    return Padding(
      padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 20.0),
      child: TextFormField(
        controller: _emailController,
        validator: (value) {
          if (value!.length == 0) {
            return 'Email can not be empty';
          } else {
            if (!value.contains('@')) {
              return 'Invalid Email';
            } else {
              return null;
            }
          }
        },
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          filled: true,
          fillColor: primaryRed.withOpacity(0.05),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(10.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(10.0),
          ),
          labelText: 'Email',
          labelStyle: TextStyle(color: primaryRed),
        ),
      ),
    );
  }

  Widget passwordField() {
    return Padding(
      padding:
          EdgeInsets.only(left: 15.0, right: 15.0, bottom: 10.0, top: 20.0),
      child: TextFormField(
        controller: _passwordController,
        validator: (value) {
          if (value!.length < 6) {
            if (value.length == 0) {
              return 'Password can not be empty';
            } else {
              return 'Password too short';
            }
          } else {
            return null;
          }
        },
        keyboardType: TextInputType.text,
        obscureText: _isHidden == true ? true : false,
        decoration: InputDecoration(
          filled: true,
          fillColor: primaryRed.withOpacity(0.05),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(10.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(10.0),
          ),
          suffixIcon: IconButton(
            onPressed: _toggleVisibility,
            icon: _isHidden
                ? Text(
                    "Show",
                    style: TextStyle(
                      fontSize: 10.0,
                    ),
                  )
                : Text(
                    "Hide",
                    style: TextStyle(
                      fontSize: 10.0,
                    ),
                  ),
          ),
          labelText: 'Password',
          labelStyle: TextStyle(color: primaryRed),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    debugPaintSizeEnabled = false;
    var myModel = Provider.of<AppConfig>(context).appModel == null
        ? null
        : Provider.of<AppConfig>(context).appModel;

    var type = MediaQuery.of(context).size.width;
    return Shortcuts(
      shortcuts: <LogicalKeySet, Intent>{
        LogicalKeySet(LogicalKeyboardKey.select): ActivateIntent(),
      },
      child: type > 900
          ? Scaffold(
              body: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    flex: 1,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        registerHereText(context),
                        logoImage(context, myModel, 0.9, 63.0, 200.0),
                      ],
                    ),
                  ),
                  Expanded(
                      flex: 1,
                      child: Padding(
                        padding: EdgeInsets.only(right: 20.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Form(
                              key: _formKey,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Expanded(
                                    flex: 1,
                                    child: emailField(),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: passwordField(),
                                  )
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 15.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  TextButton(
                                    onPressed: resetPasswordAlertBox,
                                    child: Text(
                                      'Forgot Password ?',
                                      style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w400,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 15.0),
                              child: Row(
                                children: [
                                  Expanded(
                                    flex: 1,
                                    child: ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5.0),
                                        ),
                                        backgroundColor: primaryRed,
                                        padding: EdgeInsets.symmetric(
                                          vertical: 15.0,
                                        ),
                                      ),
                                      child: _isLoading == true
                                          ? SizedBox(
                                              height: 20.0,
                                              width: 20.0,
                                              child: CircularProgressIndicator(
                                                strokeWidth: 2.0,
                                                valueColor:
                                                    AlwaysStoppedAnimation(
                                                  primaryRed,
                                                ),
                                              ),
                                            )
                                          : Text(
                                              'SIGN IN',
                                              style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.white,
                                              ),
                                            ),
                                      onPressed:
                                          _isLoading == true ? null : _saveForm,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ))
                ],
              ),
            )
          : Scaffold(
              backgroundColor: Theme.of(context).primaryColorDark,
              appBar: AppBar(
                leading: BackButton(
                  onPressed: () =>
                      Navigator.pushNamed(context, RoutePaths.loginHome),
                ),
                title: Text(
                  "Login",
                  style: TextStyle(
                    fontSize: 16.0,
                    letterSpacing: 0.9,
                  ),
                ),
                centerTitle: true,
                backgroundColor: Theme.of(context).primaryColorDark,
              ),
              body: Column(
                children: [
                  Flexible(
                    flex: 1,
                    child: Column(
                      children: [
                        Flexible(
                          flex: 1,
                          child: logoImage(context, myModel, 0.9, 63.0, 200.0),
                        ),
                      ],
                    ),
                  ),
                  Flexible(
                    flex: 4,
                    child: Container(
                      padding: EdgeInsets.only(top: 20.0),
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColorLight,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20.0),
                          topRight: Radius.circular(20.0),
                        ),
                      ),
                      child: ListView(
                        children: [
                          Form(
                            key: _formKey,
                            child: Column(
                              children: [
                                emailField(),
                                passwordField(),
                                Padding(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 15.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      TextButton(
                                        onPressed: resetPasswordAlertBox,
                                        child: Text(
                                          'Forgot Password ?',
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 16,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Padding(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 15.0),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        flex: 1,
                                        child: ElevatedButton(
                                          style: ElevatedButton.styleFrom(
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(5.0),
                                            ),
                                            backgroundColor: primaryRed,
                                            padding: EdgeInsets.symmetric(
                                              vertical: 15.0,
                                            ),
                                          ),
                                          child: _isLoading == true
                                              ? SizedBox(
                                                  height: 20.0,
                                                  width: 20.0,
                                                  child:
                                                      CircularProgressIndicator(
                                                    strokeWidth: 2.0,
                                                    valueColor:
                                                        AlwaysStoppedAnimation(
                                                      primaryRed,
                                                    ),
                                                  ),
                                                )
                                              : Text(
                                                  'SIGN IN',
                                                  style: TextStyle(
                                                    fontSize: 16,
                                                    color: Colors.white,
                                                  ),
                                                ),
                                          onPressed: _isLoading == true
                                              ? null
                                              : _saveForm,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 40,
                          ),
                          registerHereText(context),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
    );
  }
}

class LwaButtonCustom extends StatefulWidget {
  final VoidCallback onPressed;

  const LwaButtonCustom({
    Key? key,
    required this.onPressed,
  }) : super(key: key);

  @override
  _LwaButtonCustomState createState() => _LwaButtonCustomState();
}

class _LwaButtonCustomState extends State<LwaButtonCustom> {
  static const String btnImageUnpressed =
      'assets/btnlwa_gold_loginwithamazon.png';
  static const String btnImagePressed =
      'assets/btnlwa_gold_loginwithamazon_pressed.png';
  String _btnImage = btnImageUnpressed;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: (tap) {
        setState(() {
          setState(() {
            _btnImage = btnImagePressed;
          });
        });
      },
      onTapUp: (tap) {
        setState(() {
          setState(() {
            _btnImage = btnImageUnpressed;
          });
        });
      },
      child: Container(
        child: IconButton(
          icon: Image(image: AssetImage(_btnImage)),
          iconSize: 40,
          onPressed: widget.onPressed,
        ),
      ),
    );
  }
}
