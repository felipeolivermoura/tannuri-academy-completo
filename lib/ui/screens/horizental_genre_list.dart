import 'dart:math';
import 'package:flutter/material.dart';
import 'package:tannuri/common/apipath.dart';
import '../../services/countryProvider.dart';
import '/common/global.dart';
import '/common/route_paths.dart';
import '/models/comment.dart';
import '/models/datum.dart';
import '/models/genre_model.dart';
import '/providers/main_data_provider.dart';
import '/providers/movie_tv_provider.dart';
import '/ui/screens/video_grid_screen.dart';
import 'package:provider/provider.dart';

class HorizontalGenreList extends StatefulWidget {
  HorizontalGenreList({this.loading});
  final loading;
  @override
  _HorizontalGenreListState createState() => _HorizontalGenreListState();
}

class _HorizontalGenreListState extends State<HorizontalGenreList> {
  MainProvider genresProvider = MainProvider();
  List<Genre> genres = [];
  List<Datum> genreDataList = [];
  List<Datum> movieTvList = [];

  @override
  void initState() {
    super.initState();
    movieTvList =
        Provider.of<MovieTVProvider>(context, listen: false).movieTvList;
    genresProvider = Provider.of<MainProvider>(context, listen: false);
    genres = shuffle(genresProvider.genreList);
  }

  List<Genre> shuffle(List<Genre> items) {
    items = filter(items);

    var random = new Random();

    // Go through all elements.
    for (var i = items.length - 1; i > 0; i--) {
      // Pick a pseudorandom number according to the list length
      var n = random.nextInt(i + 1);

      var temp = items[i];
      items[i] = items[n];
      items[n] = temp;
    }

    return items;
  }

  List<Genre> filter(List<Genre> genres) {
    List<dynamic> removeIds = [];

    genres.forEach((genre) {
      bool isIdFound = false;
      movieTvList.forEach((movieTv) {
        movieTv.genre?.forEach((element) {
          if (genre.id.toString() == element) {
            isIdFound = true;
          }
        });
      });
      if (!isIdFound) {
        removeIds.add(genre.id);
      }
    });

    if (removeIds.isNotEmpty) {
      removeIds.forEach((id) {
        genres.removeWhere((element) => element.id == id);
      });
    }

    return genres;
  }

  getGenreData(id, movieTvList, genreList) {
    genreDataList = [];
    for (int p = 0; p < movieTvList.length; p++) {
      for (int s = 0; s < movieTvList[p].genre.length; s++) {
        if ("${movieTvList[p].genre[s]}" == "$id") {
          var genreData = movieTvList[p].genreId == null
              ? null
              : movieTvList[p].genreId.split(",").toList();
          genreDataList.add(Datum(
            isKids: movieTvList[p].isKids,
            id: movieTvList[p].id,
            actorId: movieTvList[p].actorId,
            title: movieTvList[p].title,
            trailerUrl: movieTvList[p].trailerUrl,
            status: movieTvList[p].status,
            keyword: movieTvList[p].keyword,
            description: movieTvList[p].description,
            duration: movieTvList[p].duration,
            thumbnail: movieTvList[p].thumbnail,
            poster: movieTvList[p].poster,
            directorId: movieTvList[p].directorId,
            detail: movieTvList[p].detail,
            rating: movieTvList[p].rating,
            maturityRating: movieTvList[p].maturityRating,
            subtitle: movieTvList[p].subtitle,
            subtitles: movieTvList[p].subtitles,
            publishYear: movieTvList[p].publishYear,
            released: movieTvList[p].released,
            uploadVideo: movieTvList[p].uploadVideo,
            aLanguage: movieTvList[p].aLanguage,
            createdBy: movieTvList[p].createdBy,
            createdAt: movieTvList[p].createdAt,
            updatedAt: movieTvList[p].updatedAt,
            isUpcoming: movieTvList[p].isUpcoming,
            userRating: movieTvList[p].userRating,
            movieSeries: movieTvList[p].movieSeries,
            videoLink: movieTvList[p].videoLink,
            country: movieTvList[p].country,
            genre: List.generate(genreData == null ? 0 : genreData.length,
                (int genreIndex) {
              return "${genreData[genreIndex]}";
            }),
            genres: List.generate(genreList.length, (int gIndex) {
              var genreId2 = genreList[gIndex].id.toString();
              var genreNameList = List.generate(
                  genreData == null ? 0 : genreData.length, (int nameIndex) {
                return "${genreData[nameIndex]}";
              });
              var isAv2 = 0;
              for (var y in genreNameList) {
                if (genreId2 == y) {
                  isAv2 = 1;
                  break;
                }
              }
              if (isAv2 == 1) {
                if (genreList[gIndex].name == null) {
                  return null;
                } else {
                  return "${genreList[gIndex].name}";
                }
              }
              return null;
            }),
            comments: List.generate(
                movieTvList[p].comments == null
                    ? 0
                    : movieTvList[p].comments.length, (cIndex) {
              return Comment(
                id: movieTvList[p].comments[cIndex].id,
                name: movieTvList[p].comments[cIndex].name,
                email: movieTvList[p].comments[cIndex].email,
                movieId: movieTvList[p].comments[cIndex].movieId,
                comment: movieTvList[p].comments[cIndex].comment,
                createdAt: movieTvList[p].comments[cIndex].createdAt,
                updatedAt: movieTvList[p].comments[cIndex].updatedAt,
              );
            }),
            episodeRuntime: movieTvList[p].episodeRuntime,
            genreId: movieTvList[p].genreId,
          ));
        }
      }
    }
    genreDataList.removeWhere((element) =>
        element.status == 0 ||
        "${element.status}" == "0" ||
        element.country?.contains(countryName.toUpperCase()) == true);
  }

  @override
  Widget build(BuildContext context) {
    genres.sort((a, b) => a.id.compareTo(b.id));

    if (widget.loading == true) {
      return LoadingWidget();
    } else {
      return Container(
        height: Constants.genreListHeight,
        child: ListView.builder(
          padding: EdgeInsets.only(left: 15.0, right: 5.0),
          scrollDirection: Axis.horizontal,
          itemCount: genres.length,
          itemBuilder: (BuildContext context, int index) {
            String genreImage =
                Uri.encodeComponent(genres[index].image as String);

            return Container(
              margin: EdgeInsets.only(
                right: Constants.genreItemRightMargin,
              ),
              width: Constants.genreItemWidth,
              height: Constants.genreItemHeight,
              child: Material(
                shadowColor: Colors.black,
                child: InkWell(
                  onTap: () {
                    getGenreData(genres[index].id, movieTvList, genres);
                    Navigator.pushNamed(
                      context,
                      RoutePaths.genreVideos,
                      arguments: VideoGridScreen(
                          genres[index].id, genres[index].name, genreDataList),
                    );
                  },
                ),
                color: Colors.transparent,
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5.0),
                image: DecorationImage(
                  image: NetworkImage(
                      "${APIData.domainLink}images/genre/${genreImage}"),
                  fit: BoxFit.fitWidth,
                ),
              ),
            );
          },
        ),
      );
    }
  }
}

class LoadingWidget extends StatelessWidget {
  const LoadingWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: Constants.genreListHeight,
      child: ListView.builder(
        padding: EdgeInsets.only(left: 15.0, right: 5.0),
        scrollDirection: Axis.horizontal,
        itemCount: 4,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            margin: EdgeInsets.only(right: Constants.genreItemRightMargin),
            width: Constants.genreItemWidth,
            height: Constants.genreItemHeight,
            child: Material(
              color: Colors.transparent,
              borderRadius: BorderRadius.circular(5.0),
              child: ClipRRect(
                borderRadius: new BorderRadius.circular(5.0),
                child: Image.asset(
                  "assets/placeholder_cover.jpg",
                  height: Constants.genreListHeight,
                  fit: BoxFit.fitWidth,
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
