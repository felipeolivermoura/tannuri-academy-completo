import 'dart:io';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tannuri/providers/user_playlist_provider.dart';
import 'package:tannuri/ui/shared/appbar.dart';
import '/common/apipath.dart';
import '/common/global.dart';
import '/common/route_paths.dart';
import '/models/comment.dart';
import '/models/datum.dart';
import '/providers/main_data_provider.dart';
import '/providers/movie_tv_provider.dart';
import '/providers/wishlist_provider.dart';
import '/ui/screens/blank_wishlist.dart';
import '/ui/screens/video_detail_screen.dart';
import 'package:provider/provider.dart';

class WishListScreen extends StatefulWidget {
  final int playlistId;
  final String playlistName;
  WishListScreen(this.playlistId, this.playlistName);

  @override
  _WishListScreenState createState() => _WishListScreenState();
}

class _WishListScreenState extends State<WishListScreen> {
  bool _visible = false;

  fetchWishList() async {
    tvWishList = [];
    moviesWishList = [];

    final moviesTvList =
        Provider.of<MovieTVProvider>(context, listen: false).movieTvList;
    final genreList =
        Provider.of<MainProvider>(context, listen: false).genreList;
    final myWishList = Provider.of<WishListProvider>(context, listen: false);
    await myWishList.getWishList(context, widget.playlistId);
    final mWishList = Provider.of<WishListProvider>(context, listen: false)
        .wishListModel!
        .wishlist;
    for (int i = 0; i < moviesTvList.length; i++) {
      for (int j = 0; j < mWishList!.length; j++) {
        if ("${moviesTvList[i].id}" == "${mWishList[j].movieId}") {
          var genreData = moviesTvList[i].genreId == null
              ? null
              : moviesTvList[i].genreId!.split(",").toList();

          moviesWishList.add(Datum(
            isKids: moviesTvList[i].isKids,
            id: moviesTvList[i].id,
            actorId: moviesTvList[i].actorId,
            title: moviesTvList[i].title,
            trailerUrl: moviesTvList[i].trailerUrl,
            status: moviesTvList[i].status,
            keyword: moviesTvList[i].keyword,
            description: moviesTvList[i].description,
            duration: moviesTvList[i].duration,
            thumbnail: moviesTvList[i].thumbnail,
            poster: moviesTvList[i].poster,
            directorId: moviesTvList[i].directorId,
            detail: moviesTvList[i].detail,
            rating: moviesTvList[i].rating,
            maturityRating: moviesTvList[i].maturityRating,
            subtitle: moviesTvList[i].subtitle,
            subtitles: moviesTvList[i].subtitles,
            publishYear: moviesTvList[i].publishYear,
            released: moviesTvList[i].released,
            uploadVideo: moviesTvList[i].uploadVideo,
            aLanguage: moviesTvList[i].aLanguage,
            createdBy: moviesTvList[i].createdBy,
            createdAt: moviesTvList[i].createdAt,
            updatedAt: moviesTvList[i].updatedAt,
            isUpcoming: moviesTvList[i].isUpcoming,
            userRating: moviesTvList[i].userRating,
            movieSeries: moviesTvList[i].movieSeries,
            videoLink: moviesTvList[i].videoLink,
            country: moviesTvList[i].country,
            genre: List.generate(genreData == null ? 0 : genreData.length,
                (int genreIndex) {
              return "${genreData![genreIndex]}";
            }),
            genres: List.generate(genreList.length, (int gIndex) {
              var genreId2 = genreList[gIndex].id.toString();
              var genreNameList = List.generate(
                  genreData == null ? 0 : genreData.length, (int nameIndex) {
                return "${genreData![nameIndex]}";
              });
              var isAv2 = 0;
              for (var y in genreNameList) {
                if (genreId2 == y) {
                  isAv2 = 1;
                  break;
                }
              }
              if (isAv2 == 1) {
                if (genreList[gIndex].name == null) {
                  return null;
                } else {
                  return "${genreList[gIndex].name}";
                }
              }
              return null;
            }),
            comments: List.generate(
                moviesTvList[i].comments!.isEmpty
                    ? 0
                    : moviesTvList[i].comments!.length, (cIndex) {
              return Comment(
                id: moviesTvList[i].comments![cIndex].id,
                name: moviesTvList[i].comments![cIndex].name,
                email: moviesTvList[i].comments![cIndex].email,
                movieId: moviesTvList[i].comments![cIndex].movieId,
                comment: moviesTvList[i].comments![cIndex].comment,
                subcomments: moviesTvList[i].comments![cIndex].subcomments,
                createdAt: moviesTvList[i].comments![cIndex].createdAt,
                updatedAt: moviesTvList[i].comments![cIndex].updatedAt,
              );
            }),
            episodeRuntime: moviesTvList[i].episodeRuntime,
            genreId: moviesTvList[i].genreId,
            tmdbId: moviesTvList[i].tmdbId,
            fetchBy: moviesTvList[i].fetchBy,
          ));
        }
      }
    }
    setState(() {
      _visible = true;
    });
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      _visible = false;
    });
    WidgetsBinding.instance.addPostFrameCallback((timestamp) {
      fetchWishList();
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Theme.of(context).primaryColorDark,
        body: DefaultTabController(
          initialIndex: 0,
          length: 2,
          child: SafeArea(
            child: Scaffold(
              appBar: customAppBar(context, "${widget.playlistName}")
                  as PreferredSizeWidget?,
              body: MoviesWishList(_visible, widget.playlistId),
            ),
          ),
        ),
      ),
    );
  }
}

class MoviesWishList extends StatefulWidget {
  MoviesWishList(this._visible, this.playlistId);
  final bool _visible;
  final int playlistId;

  @override
  _MoviesWishListState createState() => _MoviesWishListState();
}

class _MoviesWishListState extends State<MoviesWishList> {
  Future<String?> removeWishList(vType, id, playlistId) async {
    UserPlaylistProvider userPlaylistProvider =
        Provider.of<UserPlaylistProvider>(context, listen: false);

    final response = await http.get(
        Uri.parse(
            "${APIData.removeWatchlistMovie}$id?secret=" + APIData.secretKey),
        headers: {HttpHeaders.authorizationHeader: "Bearer $authToken"});
    if (response.statusCode == 200) {
      moviesWishList.removeWhere((element) => element.id == id);

      await http.delete(Uri.parse("${APIData.userPlaylistsApi}/wishlist/$id"),
          headers: {HttpHeaders.authorizationHeader: "Bearer $authToken"});

      await userPlaylistProvider.getUserPlaylist(context);
      setState(() {});
    }
    return null;
  }

  // PlaceHolder image displayed on the watchlist item.
  Widget placeHolderImage(movies) {
    return Expanded(
      flex: 5,
      child: Container(
        child: new ClipRRect(
          borderRadius: new BorderRadius.circular(8.0),
          child: movies.thumbnail != 'null' && movies.thumbnail != null
              ? FadeInImage.assetNetwork(
                  image: "${APIData.movieImageUri}${movies.thumbnail}",
                  placeholder: "assets/placeholder_box.jpg",
                  height: 115.0,
                  fit: BoxFit.cover,
                )
              : Container(
                  height: 115,
                  width: 100.0,
                  color: Colors.white54,
                ),
        ),
      ),
    );
  }

  Widget watchlistItemDetails(movies, genres, int playlistId) {
    return Expanded(
      flex: 6,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 6.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Flexible(
                flex: 4,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      movies.title,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w800,
                      ),
                      textAlign: TextAlign.left,
                    ),
                    SizedBox(height: 4.0),
                    Text(
                      '$genres',
                      style: TextStyle(
                        color: Color.fromRGBO(72, 163, 198, 1.0),
                        fontSize: 12.0,
                        fontWeight: FontWeight.w800,
                      ),
                      textAlign: TextAlign.left,
                      maxLines: 1,
                    ),
                  ],
                ),
              ),
              Flexible(
                flex: 1,
                child: IconButton(
                    icon: Icon(
                      CupertinoIcons.delete_simple,
                      size: 25.0,
                    ),
                    onPressed: () {
                      removeWishList(movies.type, movies.id, playlistId);
                    }),
              )
            ],
          ),
          SizedBox(height: 25.0),
          Padding(
            padding: EdgeInsets.only(right: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Flexible(
                  flex: 1,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: 20.0,
                      ),
                      Text(
                        translate('RUNTIME_'),
                        style: TextStyle(
                          fontSize: 10.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        width: 4.0,
                        height: 4.0,
                      ),
                      Text(
                        movies.duration != null
                            ? movies.duration != "0" && movies.duration != 0
                                ? movies.duration
                                : "N/A"
                            : "N/A",
                        style: TextStyle(
                          fontSize: 12.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  //  Watchlist item all details like image, name,
  Widget watchlistItemContainer(movies, genres, int playlistId) {
    return Container(
      color: Colors.transparent,
      margin: new EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 8.0),
      child: InkWell(
        onTap: () {
          Navigator.pushNamed(context, RoutePaths.videoDetail,
              arguments: VideoDetailScreen(movies));
        },
        child: Container(
          decoration: new BoxDecoration(
            color: Theme.of(context).primaryColor.withOpacity(0.1),
            shape: BoxShape.rectangle,
            borderRadius: new BorderRadius.circular(8.0),
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              placeHolderImage(movies),
              SizedBox(
                width: 10.0,
                height: 0.0,
              ),
              watchlistItemDetails(movies, genres, playlistId),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return widget._visible == false
        ? Center(
            child: CircularProgressIndicator(
              color: Theme.of(context).primaryColor,
            ),
          )
        : moviesWishList.length == 0
            ? BlankWishList()
            : Container(
                child: ListView.builder(
                  itemCount: moviesWishList.length,
                  padding:
                      EdgeInsets.symmetric(horizontal: 15.0, vertical: 0.0),
                  itemBuilder: (BuildContext context, int index) {
                    moviesWishList[index]
                        .genres!
                        .removeWhere((value) => value == null);
                    String genres = moviesWishList[index].genres.toString();
                    genres = genres.replaceAll("[", "").replaceAll("]", "");
                    return watchlistItemContainer(
                        moviesWishList[index], genres, widget.playlistId);
                  },
                ),
              );
  }
}
