import 'package:flutter/material.dart';
import '../../models/comment.dart';
import '../../models/datum.dart';
import '../../providers/main_data_provider.dart';
import '../../providers/movie_tv_provider.dart';
import 'package:provider/provider.dart';
import '../../services/countryProvider.dart';
import 'genrewise-movielist.dart';
import 'package:flutter_translate/flutter_translate.dart';

class AllHorizontalGenreList extends StatefulWidget {
  AllHorizontalGenreList();
  @override
  _AllHorizontalGenreListState createState() => _AllHorizontalGenreListState();
}

class _AllHorizontalGenreListState extends State<AllHorizontalGenreList> {
  List<Datum> genreDataList = [];

  getGenreData(id, movieTvList, genreList) {
    var genreList = Provider.of<MainProvider>(context, listen: false).genreList;
    genreDataList = [];
    for (int p = 0; p < movieTvList.length; p++) {
      for (int s = 0; s < movieTvList[p].genre.length; s++) {
        if ("${movieTvList[p].genre[s]}" == "$id") {
          var genreData = movieTvList[p].genreId == null
              ? null
              : movieTvList[p].genreId.split(",").toList();
          print("xxxxx: ${movieTvList[p].type}");
          genreDataList.add(Datum(
            isKids: movieTvList[p].isKids,
            id: movieTvList[p].id,
            actorId: movieTvList[p].actorId,
            title: movieTvList[p].title,
            trailerUrl: movieTvList[p].trailerUrl,
            status: movieTvList[p].status,
            keyword: movieTvList[p].keyword,
            description: movieTvList[p].description,
            duration: movieTvList[p].duration,
            thumbnail: movieTvList[p].thumbnail,
            poster: movieTvList[p].poster,
            directorId: movieTvList[p].directorId,
            detail: movieTvList[p].detail,
            rating: movieTvList[p].rating,
            maturityRating: movieTvList[p].maturityRating,
            subtitle: movieTvList[p].subtitle,
            subtitles: movieTvList[p].subtitles,
            publishYear: movieTvList[p].publishYear,
            released: movieTvList[p].released,
            uploadVideo: movieTvList[p].uploadVideo,
            aLanguage: movieTvList[p].aLanguage,
            createdBy: movieTvList[p].createdBy,
            createdAt: movieTvList[p].createdAt,
            updatedAt: movieTvList[p].updatedAt,
            isUpcoming: movieTvList[p].isUpcoming,
            userRating: movieTvList[p].userRating,
            movieSeries: movieTvList[p].movieSeries,
            videoLink: movieTvList[p].videoLink,
            country: movieTvList[p].country,
            genre: List.generate(genreData == null ? 0 : genreData.length,
                (int genreIndex) {
              return "${genreData[genreIndex]}";
            }),
            genres: List.generate(genreList.length, (int gIndex) {
              var genreId2 = genreList[gIndex].id.toString();
              var genreNameList = List.generate(
                  genreData == null ? 0 : genreData.length, (int nameIndex) {
                return "${genreData[nameIndex]}";
              });
              var isAv2 = 0;
              for (var y in genreNameList) {
                if (genreId2 == y) {
                  isAv2 = 1;
                  break;
                }
              }
              if (isAv2 == 1) {
                if (genreList[gIndex].name == null) {
                  return null;
                } else {
                  return "${genreList[gIndex].name}";
                }
              }
              return null;
            }),
            comments: List.generate(
                movieTvList[p].comments == null
                    ? 0
                    : movieTvList[p].comments.length, (cIndex) {
              return Comment(
                id: movieTvList[p].comments[cIndex].id,
                name: movieTvList[p].comments[cIndex].name,
                email: movieTvList[p].comments[cIndex].email,
                movieId: movieTvList[p].comments[cIndex].movieId,
                comment: movieTvList[p].comments[cIndex].comment,
                createdAt: movieTvList[p].comments[cIndex].createdAt,
                updatedAt: movieTvList[p].comments[cIndex].updatedAt,
              );
            }),
            episodeRuntime: movieTvList[p].episodeRuntime,
            genreId: movieTvList[p].genreId,
          ));
        }
      }
    }
    genreDataList.removeWhere((element) =>
        element.status == 0 ||
        "${element.status}" == "0" ||
        element.country?.contains(countryName.toUpperCase()) == true);
    return genreDataList;
  }

  @override
  Widget build(BuildContext context) {
    var genres = Provider.of<MainProvider>(context, listen: false).genreList;
    var movieTvList = Provider.of<MovieTVProvider>(context).movieTvList;

    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Theme.of(context).primaryColorDark,
      appBar: AppBar(
        title: Text(
          translate("All_Movies"),
          style: TextStyle(
            fontSize: 16.0,
            color: Colors.white,
            letterSpacing: 0.9,
          ),
        ),
        centerTitle: true,
        backgroundColor: Theme.of(context).primaryColorDark.withOpacity(0.8),
      ),
      body: ListView.builder(
          scrollDirection: Axis.vertical,
          itemCount: genres.length,
          itemBuilder: (BuildContext context, int index) {
            getGenreData(genres[index].id, movieTvList, genres);
            return genreDataList.length > 0
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding:
                            EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0),
                        child: Text(
                          genres[index].name.toString(),
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              fontFamily: 'Lato',
                              fontSize: 16.0,
                              fontWeight: FontWeight.w700,
                              color: Colors.white),
                        ),
                      ),
                      GenreWiseMoviesList(
                          genres[index].id, genres[index].name, genreDataList)
                    ],
                  )
                : SizedBox.shrink();
          }),
    );
  }
}
