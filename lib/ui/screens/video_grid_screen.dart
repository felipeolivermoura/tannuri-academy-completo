import 'dart:convert';
import 'dart:io';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:tannuri/common/apipath.dart';
import 'package:tannuri/common/global.dart';
import 'package:tannuri/models/category_filter_model.dart';
import 'package:tannuri/providers/categories_provider.dart';
import 'package:tannuri/ui/shared/checkbox_widget.dart';
import 'package:tannuri/ui/shared/heading1.dart';
import '/models/datum.dart';
import '/ui/shared/appbar.dart';
import '/ui/shared/grid_video_container.dart';

// ignore: must_be_immutable
class VideoGridScreen extends StatefulWidget {
  final dynamic id;
  final String? title;
  final List<Datum> genreDataList;

  VideoGridScreen(this.id, this.title, this.genreDataList);

  @override
  State<VideoGridScreen> createState() => _VideoGridScreenState();
}

class _VideoGridScreenState extends State<VideoGridScreen> {
  late CategoryProvider categoriesFilter =
      Provider.of<CategoryProvider>(context, listen: false);

  List<Category> categories = [];
  List<CheckBoxModel> categoriesCheck = [];
  List<int> checkedSubIds = [];
  List<int> checkedIds = [];
  List<Datum> filterDataList = [];
  List<dynamic> categoriesList = [];

  List<int> catIds = [];
  List<int> catSubIds = [];

  bool isLoading = true;

  Future<void> getCategories() async {
    CategoryProvider categoriesProvider =
        Provider.of<CategoryProvider>(context, listen: false);

    await categoriesProvider.getCategories(context, "${widget.id}");

    setState(() {
      if (categoriesFilter.categoryModel != null) {
        categories =
            categoriesFilter.categoryModel!.categories as List<Category>;

        catIds.clear();
        catSubIds.clear();
        categories.forEach((c) {
          final checkExists = categoriesCheck
              .where((check) => check.id == c.id && c.title != "");
          if (checkExists.isEmpty) {
            final checkBox = CheckBoxModel(id: c.id, title: c.title, subs: []);

            c.subs.forEach((sc) {
              final subCheckExists =
                  checkBox.subs.where((check) => check.id == sc.id);
              if (subCheckExists.isEmpty) {
                checkBox.subs.add(CheckSubBoxModel(id: sc.id, title: sc.title));
              }
              catSubIds.add(sc.id);
            });

            catIds.add(c.id);
            categoriesCheck.add(checkBox);
          }
        });
      }
    });
  }

  Future<void> getCategoriesMovies() async {
    List<int> cIds = [];
    List<int> sCIds = [];

    categoriesCheck.forEach((item) {
      cIds.add(item.id);
      item.subs.forEach((sub) => sCIds.add(sub.id));
    });

    final bodyData = {
      "genre_id": "${widget.id}",
      "cats": "${cIds.join(',')}",
      "subCats": "${sCIds.join(',')}",
    };

    Locale myLocale = Localizations.localeOf(context);
    final response = await http.post(
        Uri.parse("${APIData.filterMoviesApi}&lang=${myLocale}"),
        body: bodyData,
        headers: {HttpHeaders.authorizationHeader: "Bearer $authToken"});

    if (response.statusCode == 200) {
      String data = response.body;
      categoriesList = json.decode(data)['movie'];

      setState(() {
        if (categoriesFilter.categoryModel != null) {
          categories =
              categoriesFilter.categoryModel!.categories as List<Category>;
        }
      });
    }
  }

  Widget filterMenu() {
    return Padding(
      padding: const EdgeInsets.only(top: 0),
      child: Padding(
        padding: const EdgeInsets.only(right: 5.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            InkWell(
              child: Container(
                width: 40,
                height: 40,
                child: Icon(
                  Icons.filter_alt_outlined,
                ),
              ),
              onTap: () => filtersDialog(),
            ),
          ],
        ),
      ),
    );
  }

  Widget applyFilter() {
    return Padding(
      padding: const EdgeInsets.only(top: 0),
      child: Padding(
        padding: const EdgeInsets.only(right: 5.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            InkWell(
              child: Container(
                width: 40,
                height: 40,
                child: Icon(
                  Icons.done,
                ),
              ),
              onTap: () => sendFilter(),
            ),
          ],
        ),
      ),
    );
  }

  void sendFilter() async {
    List<CheckBoxModel> itensMarcados =
        List.from(categoriesCheck.where((item) => item.checked));
    checkedSubIds.clear();
    checkedIds.clear();

    itensMarcados.forEach((item) {
      checkedIds.add(item.id);
      item.subs
          .where((s) => s.checked)
          .forEach((sub) => checkedSubIds.add(sub.id));
    });

    setState(() {
      isLoading = true;
    });

    Navigator.of(context).pop();
    String cats =
        checkedIds.isNotEmpty ? checkedIds.join(',') : catIds.join(',');
    String subCats = checkedSubIds.isNotEmpty
        ? checkedSubIds.join(',')
        : catSubIds.join(',');

    final bodyData = {
      "genre_id": "${widget.id}",
      "cats": "${cats}",
      "subCats": "${subCats}",
    };

    Locale myLocale = Localizations.localeOf(context);
    final response = await http.post(
        Uri.parse("${APIData.filterMoviesApi}&lang=${myLocale}"),
        body: bodyData,
        headers: {HttpHeaders.authorizationHeader: "Bearer $authToken"});

    if (response.statusCode == 200) {
      String data = response.body;
      categoriesList = json.decode(data)['movie'];

      setState(() {
        isLoading = false;
      });
    }
  }

  Future<void> filtersDialog() => showGeneralDialog(
        context: context,
        pageBuilder: (
          BuildContext context,
          Animation<double> animation,
          Animation<double> secondaryAnimation,
        ) {
          return Center(
            child: Container(
              color: Colors.white,
              child: Scaffold(
                appBar: customAppBar(
                  context,
                  "Filtros",
                  applyFilter(),
                ) as PreferredSizeWidget?,
                body: ListView.builder(
                  itemCount: categoriesCheck.length,
                  itemBuilder: (_, int index) {
                    return Column(
                      children: [
                        CheckboxWidget(item: categoriesCheck[index]),
                      ],
                    );
                  },
                ),
              ),
            ),
          );
        },
      );

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      await getCategories();
      await getCategoriesMovies();

      setState(() {
        isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    List<CheckBoxModel> checkeds =
        List.from(categoriesCheck.where((c) => c.checked));
    checkeds = checkeds.isNotEmpty ? checkeds : categoriesCheck;

    return Scaffold(
      appBar: customAppBar(context, widget.title,
          categories.length > 0 ? filterMenu() : null) as PreferredSizeWidget?,
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(
                color: Theme.of(context).primaryColor,
              ),
            )
          : SingleChildScrollView(
              child: Column(
                children: [
                  for (int index = 0; index < checkeds.length; index += 1)
                    CategoryListWidget(
                      title: checkeds[index].title,
                      dataList: index,
                      list: widget.genreDataList.asMap().containsKey(index)
                          ? categoriesList[index]
                          : [],
                      genreDataList: widget.genreDataList,
                    ),
                ],
              ),
            ),
    );
  }
}

class CategoryListWidget extends StatelessWidget {
  const CategoryListWidget({
    Key? key,
    required this.title,
    required this.dataList,
    required this.list,
    required this.genreDataList,
  }) : super(key: key);

  final String title;
  final int dataList;
  final List<dynamic> list;
  final List<Datum> genreDataList;

  @override
  Widget build(BuildContext context) {
    final List<Datum> dataList = List<Datum>.from(
      list.map((x) => Datum.fromJson(x)).where(
            (element) =>
                (genreDataList.firstWhereOrNull((g) => g.id == element.id) !=
                    null),
          ),
    );

    List<Widget> videoList =
        List.generate(dataList.isEmpty ? 0 : dataList.length, (index) {
      final Datum genreData =
          genreDataList.firstWhere((g) => g.id == dataList[index].id);

      return GridVideoContainer(context, genreData);
    });

    return videoList.isNotEmpty
        ? Column(
            children: [
              Heading1(title, "CAT", false),
              GridView.count(
                padding: EdgeInsets.only(
                  left: 15.0,
                  right: 15.0,
                  top: 15.0,
                  bottom: 15.0,
                ),
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                physics: ClampingScrollPhysics(),
                crossAxisCount: 3,
                childAspectRatio: 18 / 28,
                crossAxisSpacing: 10.0,
                mainAxisSpacing: 8.0,
                children: videoList,
              ),
            ],
          )
        : Container();
  }
}
