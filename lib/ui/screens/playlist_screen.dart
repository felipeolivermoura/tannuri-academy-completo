import 'dart:convert';
import 'dart:io';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tannuri/common/route_paths.dart';
import 'package:tannuri/models/user_playlist_model.dart';
import 'package:tannuri/providers/user_playlist_provider.dart';
import 'package:tannuri/ui/screens/wishlist_screen.dart';
import '/common/apipath.dart';
import '/common/global.dart';
import '/ui/screens/blank_wishlist.dart';

class PlaylistScreen extends StatefulWidget {
  @override
  _PlaylistScreenState createState() => _PlaylistScreenState();
}

class _PlaylistScreenState extends State<PlaylistScreen> {
  fetchWishList() async {
    moviesWishList = [];
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timestamp) {
      fetchWishList();
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Theme.of(context).primaryColorDark,
        body: DefaultTabController(
          initialIndex: 0,
          length: 2,
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  centerTitle: true,
                  title: Text(
                    translate("Playlists_"),
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 18.0,
                      letterSpacing: 0.9,
                    ),
                  ),
                  backgroundColor: Theme.of(context).primaryColorDark,
                  floating: true,
                  pinned: true,
                  automaticallyImplyLeading: false,
                ),
              ];
            },
            body: Playlists(),
          ),
        ),
      ),
    );
  }
}

class Playlists extends StatefulWidget {
  @override
  _PlaylistsState createState() => _PlaylistsState();
}

enum MenuItem { itemDelete, itemRename }

class _PlaylistsState extends State<Playlists> {
  MenuItem? selectedMenu;
  bool _visible = false;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      await playlistCheck(context);
    });
  }

  Future playlistCheck(BuildContext context) async {
    UserPlaylistProvider userPlaylistProvider =
        Provider.of<UserPlaylistProvider>(context, listen: false);

    await userPlaylistProvider.getUserPlaylist(context);
    setState(() {
      _visible = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    List<UserPlaylist>? userPlaylists =
        Provider.of<UserPlaylistProvider>(context, listen: true)
            .userPlaylistModel!
            .playlists;

    if (_visible == false) {
      return Center(
        child: CircularProgressIndicator(
          color: Theme.of(context).primaryColor,
        ),
      );
    } else {
      if (userPlaylists != null && userPlaylists.length == 0) {
        return BlankWishList();
      } else {
        return ReorderableListView(
          padding: const EdgeInsets.symmetric(horizontal: 35),
          children: <Widget>[
            for (int index = 0; index < userPlaylists!.length; index += 1)
              ListTile(
                  key: Key('$index'),
                  tileColor: Color(0XFF202325),
                  shape: RoundedRectangleBorder(
                    //<-- SEE HERE
                    side: BorderSide(
                      width: 4,
                      color: Theme.of(context).primaryColorDark,
                    ),
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                  leading: Container(
                    padding: EdgeInsets.all(12),
                    width: 50,
                    height: 50,
                    decoration: BoxDecoration(
                      color: Colors.black38,
                      borderRadius: BorderRadius.circular(4.0),
                    ),
                    child: Image.asset("assets/icone.png"),
                  ),
                  title: Text(
                    userPlaylists[index].name,
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  subtitle: Container(
                    child: Text(
                      userPlaylists[index].wishlist!.length == 0
                          ? "Nenhum vídeo"
                          : userPlaylists[index].wishlist!.length > 1
                              ? "${userPlaylists[index].wishlist!.length} Vídeos"
                              : "${userPlaylists[index].wishlist!.length} Vídeo",
                      style: TextStyle(
                        fontSize: 11,
                        fontWeight: FontWeight.w400,
                      ),
                      textAlign: TextAlign.start,
                    ),
                  ),
                  trailing: InkWell(
                    child: PopupMenuButton<MenuItem>(
                      initialValue: selectedMenu,
                      // Callback that sets the selected popup menu item.
                      onSelected: (MenuItem item) {
                        setState(() {
                          selectedMenu = item;
                          if (item.name == "itemDelete")
                            _deleteItem(userPlaylists[index].id);
                        });
                      },
                      itemBuilder: (BuildContext context) =>
                          <PopupMenuEntry<MenuItem>>[
                        const PopupMenuItem<MenuItem>(
                          value: MenuItem.itemDelete,
                          child: Text('Remover'),
                        ),
                      ],
                    ),
                  ),
                  onTap: () => {
                        Navigator.pushNamed(context, RoutePaths.wishlist,
                            arguments: WishListScreen(userPlaylists[index].id,
                                userPlaylists[index].name))
                      }),
          ],
          onReorder: _updateMyItems,
        );
      }
    }
  }

  void _deleteItem(int id) async {
    List<UserPlaylist>? userPlaylists =
        Provider.of<UserPlaylistProvider>(context, listen: false)
            .userPlaylistModel!
            .playlists;

    int index = userPlaylists!.indexWhere((item) => item.id == id);
    await http.delete(Uri.parse("${APIData.userPlaylistsApi}/${id}"),
        headers: {HttpHeaders.authorizationHeader: "Bearer $authToken"});

    userPlaylists.removeAt(index);
    setState(() {});
  }

  void _updateMyItems(int oldIndex, int newIndex) async {
    List<UserPlaylist>? userPlaylists =
        Provider.of<UserPlaylistProvider>(context, listen: false)
            .userPlaylistModel!
            .playlists;

    final UserPlaylist item = userPlaylists!.removeAt(oldIndex);

    int order = newIndex == 0 ? newIndex + 1 : newIndex;
    int oldOrder = oldIndex == 0 ? oldIndex + 1 : oldIndex;
    item.order = order;
    print("$oldOrder - $order");
    setState(() {
      if (oldIndex < newIndex) {
        newIndex -= 1;
      }
      userPlaylists.insert(newIndex, item);
    });

    List<dynamic> playlistIds = userPlaylists.map((element) {
      return element.id;
    }).toList();

    await http.put(Uri.parse("${APIData.userPlaylistsApi}"), body: {
      "type": 'sort',
      "playlist_ids": jsonEncode(playlistIds),
    }, headers: {
      HttpHeaders.authorizationHeader: "Bearer $authToken"
    });
  }
}

class TVSeriesWishList extends StatefulWidget {
  TVSeriesWishList(this._visible);
  final bool _visible;

  @override
  _TVSeriesWishListState createState() => _TVSeriesWishListState();
}

class _TVSeriesWishListState extends State<TVSeriesWishList> {
  @override
  Widget build(BuildContext context) {
    return widget._visible == false
        ? Center(
            child: CircularProgressIndicator(),
          )
        : BlankWishList();
  }
}
