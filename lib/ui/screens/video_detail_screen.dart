import 'dart:developer';
import 'dart:io';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tannuri/ui/shared/copy_password.dart';
import '../../models/AllUsers.dart';
import '../../providers/all_user_provider.dart';
import '/common/apipath.dart';
import '/common/global.dart';
import '/common/route_paths.dart';
import '/models/comment.dart';
import '/models/datum.dart';
import '/player/iframe_player.dart';
import '/player/m_player.dart';
import '/providers/app_config.dart';
import '/providers/menu_data_provider.dart';
import '/providers/user_profile_provider.dart';
import '/services/download/download_page.dart';
import '/ui/shared/container_border.dart';
import '/ui/shared/description_text.dart';
import '/ui/shared/rate_us.dart';
import '/ui/shared/share_page.dart';
import '/ui/shared/tab_widget.dart';
import '/ui/shared/wishlist.dart';
import '/ui/widgets/video_detail_header.dart';
import 'package:provider/provider.dart';

int episodesCounting = 0;
int cSeasonIndex = 0;

class VideoDetailScreen extends StatefulWidget {
  final Datum? videoDetail;
  VideoDetailScreen(this.videoDetail);
  @override
  _VideoDetailScreenState createState() => _VideoDetailScreenState();
}

class _VideoDetailScreenState extends State<VideoDetailScreen>
    with TickerProviderStateMixin, RouteAware {
  bool _show = true;
  double bottomBarHeight = 75; // set bottom bar height
  late UserProfileProvider userDetails;

  var mReadyUrl,
      mIFrameUrl,
      mUrl360,
      mUrl480,
      mUrl720,
      mUrl1080,
      youtubeUrl,
      vimeoUrl;
  var screenUsed1, screenUsed2, screenUsed3, screenUsed4;
  ScrollController? _scrollController;
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  late TabController _tabController;
  TextEditingController commentsController = new TextEditingController();
  TextEditingController subCommentsController = new TextEditingController();
  final _formKey = new GlobalKey<FormState>();
  final _formKey1 = new GlobalKey<FormState>();
  var dMsg = '';
  List<Comment> commentList = [
    Comment(
      id: 0,
      name: "xx",
      email: "xx",
      comment: "No Comments",
      status: 1,
      createdAt: DateTime.now(),
      updatedAt: DateTime.now(),
    )
  ];

  void showBottomBar() {
    setState(() {
      _show = true;
    });
  }

  void hideBottomBar() {
    setState(() {
      _show = false;
    });
  }

  void showAds() async {
    userDetails = Provider.of<UserProfileProvider>(context, listen: false);
    if (userDetails.userProfileModel!.removeAds == "0" ||
        userDetails.userProfileModel!.removeAds == 0) {
      showBottomBar();
    } else {
      hideBottomBar();
    }
  }

  Widget heading(heading) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 15.0),
      child: Text(
        heading,
        style: TextStyle(
          fontFamily: 'Lato',
          fontSize: 16.0,
          fontWeight: FontWeight.w700,
        ),
      ),
    );
  }

  // Sliver app bar tabs
  Widget _sliverAppBar(innerBoxIsScrolled) => SliverAppBar(
        titleSpacing: 0.00,
        elevation: 0.0,
        title: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            CustomBorder(),
            TabBar(
                onTap: (currentIndex) {
                  setState(() {
                    cSeasonIndex = currentIndex;
                  });
                },
                indicator: UnderlineTabIndicator(
                  borderSide: BorderSide(
                      color: Theme.of(context).primaryColor, width: 2.5),
                  insets: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 46.0),
                ),
                indicatorColor: Colors.orangeAccent,
                indicatorSize: TabBarIndicatorSize.tab,
                indicatorWeight: 3.0,
                indicatorPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                unselectedLabelColor: Color.fromRGBO(95, 95, 95, 1.0),
                tabs: [
                  TabWidget(translate('MORE_LIKE_THIS')),
                  TabWidget(translate('MORE_DETAILS')),
                ]),
          ],
        ),
        pinned: true,
        floating: true,
        forceElevated: innerBoxIsScrolled,
        automaticallyImplyLeading: false,
        backgroundColor: Theme.of(context).primaryColorDark,
      );

  Widget movieSliverList() {
    final platform = Theme.of(context).platform;
    final disableDownloads = true;

    return SliverList(
      delegate: SliverChildBuilderDelegate((BuildContext context, int j) {
        return Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              VideoDetailHeader(
                  widget.videoDetail, userDetails.userProfileModel),
              SizedBox(
                height: 20.0,
              ),
              widget.videoDetail!.detail == null ||
                      widget.videoDetail!.detail == ""
                  ? SizedBox.shrink()
                  : DescriptionText(widget.videoDetail!.detail),
              SizedBox(
                height: 5.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  WishListView(widget.videoDetail),
                  RateUs("M", widget.videoDetail!.id),
                  SharePage(APIData.shareMovieUri, widget.videoDetail!.id),
                  !disableDownloads
                      ? DownloadPage(widget.videoDetail!, platform)
                      : SizedBox.shrink(),
                  if (protectedContentPwd.length > 0)
                    if (protectedContentPwd.containsKey(
                            '${widget.videoDetail?.id}_${widget.videoDetail?.id}') &&
                        (userDetails.userProfileModel?.active == 1 ||
                            userDetails.userProfileModel?.active == '1'))
                      CopyPassword(widget.videoDetail!),
                ],
              ),
            ],
          ),
        );
      }, childCount: 1),
    );
  }

  //  Tab bar for similar movies or tv series
  Widget _tabBarView(moreLikeThis) {
    return TabBarView(children: <Widget>[
      new ListView(
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        children: <Widget>[
          tapOnMoreLikeThis(moreLikeThis),
        ],
      ),
      moreDetails()
    ]);
  }

  Widget tapOnMoreLikeThis(moreLikeThis) {
    return Container(
      height: 300.0,
      child: GridView.count(
        shrinkWrap: true,
        crossAxisCount: 2,
        childAspectRatio: 1.5,
        scrollDirection: Axis.horizontal,
        children: List<Padding>.generate(
          moreLikeThis == null ? 0 : moreLikeThis.length,
          (int index) {
            return Padding(
              padding: EdgeInsets.only(right: 2.5, left: 2.5, bottom: 5.0),
              child: moreLikeThis[index] == null
                  ? Container()
                  : cusPlaceHolder(moreLikeThis, index),
            );
          },
        ),
      ),
    );
  }

  Widget moreDetails() {
    var commentsStatus = Provider.of<AppConfig>(context, listen: false)
        .appModel!
        .config!
        .comments;
    widget.videoDetail!.genres!.removeWhere((value) => value == null);
    String genres = widget.videoDetail!.genres.toString();
    genres = genres.replaceAll("[", "").replaceAll("]", "");
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            width: 0.0,
          ),
          genresDetailsContainer(widget.videoDetail, genres),
          commentsStatus == 1 || "$commentsStatus" == "1"
              ? comments()
              : SizedBox.shrink(),
        ],
      ),
    );
  }

  Widget comments() {
    print("test comment $commentList");
    return Container(
      color: Theme.of(context).primaryColorLight,
      padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
      margin: EdgeInsets.symmetric(vertical: 15.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                translate("Comments_"),
                style: TextStyle(
                  fontSize: 16.0,
                ),
              ),
              ElevatedButton(
                onPressed: () {
                  addComment(context);
                },
                child: Text(
                  translate("Add_"),
                ),
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all<Color?>(
                    activeDotColor,
                  ),
                ),
              ),
            ],
          ),
          commentList.isEmpty
              ? SizedBox.shrink()
              : getCommentsList(commentList),
        ],
      ),
    );
  }

  Widget getCommentsList(List<Comment> comments) {
    AllUsers allUsers =
        Provider.of<AllUsersProvider>(context, listen: false).allUsers;

    List<Widget> list = [];

    List<Widget> subComments = [];

    for (int i = 0; i < comments.length; i++) {
      print("Comment status: ${comments[i].comment}");
      if (comments[i].status == 1 || comments[i].status == '1') {
        if (comments[i].subcomments != null && comments[i].name != 'xx') {
          subComments = [];
          comments[i].subcomments?.forEach((subComment) {
            if (subComment.status == 1 || subComment.status == '1' || true) {
              String? userName = "";
              allUsers.allusers?.forEach((user) {
                if (subComment.userId == user.id) {
                  userName = user.name;
                }
              });

              subComments.add(
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        userName!,
                        style: TextStyle(
                          fontSize: 16.0,
                        ),
                      ),
                      SizedBox(
                        height: 6.0,
                      ),
                      Text(
                        DateFormat.yMMMd().format(subComment.createdAt!),
                        style: TextStyle(
                          fontSize: 12.0,
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(
                        subComment.reply!,
                        style: TextStyle(
                          fontSize: 14.0,
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                    ],
                  ),
                ),
              );
            }
          });
        }

        list.add(
          Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  comments[i].name!,
                  style: TextStyle(
                    fontSize: 16.0,
                  ),
                ),
                SizedBox(
                  height: 6.0,
                ),
                Text(
                  DateFormat.yMMMd().format(
                    DateTime.parse("${comments[i].createdAt}"),
                  ),
                  style: TextStyle(
                    fontSize: 12.0,
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  comments[i].comment!,
                  style: TextStyle(
                    fontSize: 14.0,
                  ),
                ),
                SizedBox(
                  height: 5.0,
                ),
                if (comments[i].name != 'xx')
                  ElevatedButton(
                    onPressed: () {
                      addSubComment(context, comments[i].id);
                    },
                    child: Text(
                      translate("Reply_"),
                      style: TextStyle(
                        fontSize: 14.0,
                      ),
                    ),
                  ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: 30,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: subComments,
                    ),
                  ],
                ),
                Divider(thickness: 1),
              ],
            ),
          ),
        );
      }
    }
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: list,
    );
  }

  Future<void> addComment(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Colors.white,
          contentPadding: EdgeInsets.only(left: 25.0, right: 25.0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(25.0),
            ),
          ),
          title: Container(
            alignment: Alignment.topLeft,
            child: Text(
              translate('Add_Comments'),
              style: TextStyle(
                color: activeDotColor,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          content: Container(
            height: MediaQuery.of(context).size.height / 4,
            child: Flexible(
              child: Column(
                children: <Widget>[
                  Flexible(
                    flex: 2,
                    child: Form(
                      key: _formKey,
                      child: Container(
                        margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                        ),
                        child: TextFormField(
                          controller: commentsController,
                          maxLines: 4,
                          decoration: new InputDecoration(
                            border: InputBorder.none,
                            contentPadding: EdgeInsets.only(
                                left: 10.0, right: 10.0, top: 5.0, bottom: 5.0),
                            hintText: translate("Comment_"),
                            errorStyle: TextStyle(fontSize: 10),
                            hintStyle: TextStyle(
                                color: Colors.black.withOpacity(0.4),
                                fontSize: 18),
                          ),
                          style: TextStyle(
                              color: Colors.black.withOpacity(0.7),
                              fontSize: 18),
                          validator: (val) {
                            if (val!.length == 0) {
                              return translate("Comment_cannot_be_blank");
                            }
                            return null;
                          },
                          onSaved: (val) => commentsController.text = val!,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Flexible(
                    flex: 1,
                    child: InkWell(
                      child: Container(
                        color: activeDotColor,
                        height: 45.0,
                        width: 100.0,
                        padding: EdgeInsets.only(
                            left: 15.0, right: 15.0, top: 5.0, bottom: 5.0),
                        child: Center(
                          child: Text(
                            translate("Post_"),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                      onTap: () {
                        final form = _formKey.currentState!;
                        form.save();
                        if (form.validate() == true) {
                          postComment();
                        }
                      },
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Future<String?> postComment() async {
    var userDetails = Provider.of<UserProfileProvider>(context, listen: false)
        .userProfileModel!;
    var type = "M";
    final postCommentResponse =
        await http.post(Uri.parse(APIData.postBlogComment), body: {
      "type": '$type',
      "id": '${widget.videoDetail!.id}',
      "comment": '${commentsController.text}',
      "name": '${userDetails.user!.name}',
      "email": '${userDetails.user!.email}',
    }, headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      HttpHeaders.acceptHeader: "application/json",
      HttpHeaders.authorizationHeader: "Bearer $authToken"
    });
    print("type: $type");
    print("id: ${widget.videoDetail!.id}");
    print("comment: ${commentsController.text}");
    print("name: ${userDetails.user!.name}");
    print("email: ${userDetails.user!.email}");
    print(postCommentResponse.statusCode);
    if (postCommentResponse.statusCode == 200) {
      commentList.add(
        Comment(
          id: widget.videoDetail!.id,
          name: "${userDetails.user!.name}",
          email: "${userDetails.user!.email}",
          comment: commentsController.text,
          createdAt: DateTime.now(),
          updatedAt: DateTime.now(),
        ),
      );
      setState(() {});
      commentsController.text = '';
      Fluttertoast.showToast(msg: translate("Commented_Successfully"));
      commentsController.text = '';
      Navigator.pop(context);
    } else {
      Fluttertoast.showToast(msg: translate("Error_in_commenting"));
      commentsController.text = '';
      Navigator.pop(context);
    }
    return null;
  }

  Future<void> addSubComment(BuildContext context, var commentId) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Colors.white,
          contentPadding: EdgeInsets.only(left: 25.0, right: 25.0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(25.0),
            ),
          ),
          title: Container(
            alignment: Alignment.topLeft,
            child: Text(
              translate('Add_Reply'),
              style: TextStyle(
                color: activeDotColor,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          content: Container(
            height: MediaQuery.of(context).size.height / 4,
            child: Flexible(
              child: Column(
                children: <Widget>[
                  Flexible(
                    flex: 2,
                    child: Form(
                      key: _formKey1,
                      child: Container(
                        margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                        ),
                        child: TextFormField(
                          controller: subCommentsController,
                          maxLines: 4,
                          decoration: new InputDecoration(
                            border: InputBorder.none,
                            contentPadding: EdgeInsets.only(
                                left: 10.0, right: 10.0, top: 5.0, bottom: 5.0),
                            hintText: translate("Reply_"),
                            errorStyle: TextStyle(fontSize: 10),
                            hintStyle: TextStyle(
                                color: Colors.black.withOpacity(0.4),
                                fontSize: 18),
                          ),
                          style: TextStyle(
                              color: Colors.black.withOpacity(0.7),
                              fontSize: 18),
                          validator: (val) {
                            if (val!.length == 0) {
                              return translate("Reply_cannot_be_blank");
                            }
                            return null;
                          },
                          onSaved: (val) => subCommentsController.text = val!,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Flexible(
                    flex: 1,
                    child: InkWell(
                      child: Container(
                        color: activeDotColor,
                        height: 45.0,
                        width: 100.0,
                        padding: EdgeInsets.only(
                            left: 15.0, right: 15.0, top: 5.0, bottom: 5.0),
                        child: Center(
                          child: Text(
                            translate("Post_"),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                      onTap: () {
                        final form = _formKey1.currentState!;
                        form.save();
                        print("postSubComment called!");
                        if (form.validate() == true) {
                          postSubComment(commentId);
                        }
                      },
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Future<String?> postSubComment(var commentId) async {
    var userDetails = Provider.of<UserProfileProvider>(context, listen: false)
        .userProfileModel!;
    var type = "M";
    final response = await http.post(Uri.parse(APIData.postSubComment), body: {
      "type": '$type',
      "user_id": '${userDetails.user!.id}',
      "comment_id": '$commentId',
      "reply": '${subCommentsController.text}',
      "status": '0',
    }, headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      HttpHeaders.acceptHeader: "application/json",
      HttpHeaders.authorizationHeader: "Bearer $authToken"
    });

    print("Sub Comment Status Code : ${response.statusCode}");
    print("Sub Comment Response : ${response.body}");
    if (response.statusCode == 200) {
      setState(() {});
      Fluttertoast.showToast(msg: translate("Commented_Successfully"));
      subCommentsController.text = '';
      Navigator.pop(context);
    } else {
      Fluttertoast.showToast(msg: translate("Error_in_commenting"));
      subCommentsController.text = '';
      Navigator.pop(context);
    }
    return null;
  }

  Widget genresDetailsContainer(videoDetail, genres) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              translate("About_"),
              style: TextStyle(
                  color: Theme.of(context).hintColor,
                  fontSize: 16.0,
                  fontWeight: FontWeight.w500),
            ),
            Container(
              height: 8.0,
            ),
            genreNameRow(videoDetail),
            genresRow(genres),
          ],
        ),
      ),
      color: Theme.of(context).primaryColorLight,
    );
  }

  Widget genresRow(genres) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 2.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: Text(
              translate('Genres_'),
              style: TextStyle(
                color: Theme.of(context).hintColor,
                fontSize: 13.0,
              ),
            ),
          ),
          Expanded(
            flex: 5,
            child: GestureDetector(
              onTap: () {},
              child: Text(
                "$genres",
                style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontSize: 13.0,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget genreNameRow(videoDetail) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 2.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: Text(
              translate('Name_'),
              style: TextStyle(
                color: Theme.of(context).hintColor,
                fontSize: 13.0,
              ),
            ),
          ),
          Expanded(
            flex: 5,
            child: GestureDetector(
              onTap: () {},
              child: Text(
                "${videoDetail.title}",
                style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontSize: 13.0,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  //  Genre details text
  Widget genreDetailsText(videoDetail) {
    return Expanded(
      flex: 5,
      child: GestureDetector(
        onTap: () {},
        child: Text(
          "${videoDetail.seasons[cSeasonIndex].detail}",
          style: TextStyle(
            color: Theme.of(context).primaryColor,
            fontSize: 13.0,
          ),
        ),
      ),
    );
  }

  //  Customer also watched videos place holder
  Widget cusPlaceHolder(moreLikeThis, index) {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0, right: 8.0, bottom: 8.0),
      child: InkWell(
        child: moreLikeThis[index].thumbnail == null
            ? Container(
                height: 150,
                width: 120.0,
                color: Colors.white54,
              )
            : FadeInImage.assetNetwork(
                image:
                    "${APIData.movieImageUri}${moreLikeThis[index].thumbnail}",
                placeholder: 'assets/placeholder_box.jpg',
                height: 150.0,
                fit: BoxFit.cover,
                imageErrorBuilder: (context, error, stackTrace) {
                  return Container(
                    height: 150,
                    width: 120.0,
                    color: Colors.white54,
                  );
                },
              ),
        onTap: () {
          Navigator.pushNamed(
            context,
            RoutePaths.videoDetail,
            arguments: VideoDetailScreen(moreLikeThis[index]),
          );
        },
      ),
    );
  }

  //  Scaffold that contains overall UI of his page
  Widget scaffold(moreLikeThis) {
    return Scaffold(
      key: _scaffoldKey,
      body: _movieScrollView(moreLikeThis),
      backgroundColor: Theme.of(context).primaryColorDark,
    );
  }

  Widget _movieScrollView(moreLikeThis) {
    return NestedScrollView(
      controller: _scrollController,
      headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
        return <Widget>[
          movieSliverList(),
          _sliverAppBar(innerBoxIsScrolled),
        ];
      },
      body: _tabBarView(moreLikeThis),
    );
  }

  Widget cusAlsoWatchedText() {
    return Container(
      margin: const EdgeInsets.fromLTRB(0.0, 25.0, 0, 5.0),
      child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [heading(translate("Customers_also_watched"))]),
    );
  }

  Future<String?> addHistory(vType, id) async {
    var type = "M";
    final response = await http.get(
        Uri.parse(
            "${APIData.addWatchHistory}/$type/$id?secret=${APIData.secretKey}"),
        headers: {HttpHeaders.authorizationHeader: "Bearer $authToken"});
    log("Add to Watch History API Input :-> Type = $type, ID = $id");
    log("Add to Watch History API Status Code :-> ${response.statusCode}");
    log("Add to Watch History API Response :-> ${response.body}");
    if (response.statusCode == 200) {
    } else {
      throw "can't added to history.";
    }
    return null;
  }

  bool notAdult() {
    bool canWatch = true;
    if (widget.videoDetail!.maturityRating == MaturityRating.ADULT) {
      log('Adult Content');
      if (int.parse(userDetails.userProfileModel!.user!.age.toString()) <= 18) {
        canWatch = false;
      }
    }
    return canWatch;
  }

  freeTrial(videoURL, type, title, subtitles) {
    bool canWatch = notAdult();
    if (canWatch) {
      if (type == "EMD") {
        addHistory("M", widget.videoDetail!.id);
        var router = new MaterialPageRoute(
          builder: (BuildContext context) => IFramePlayerPage(url: mIFrameUrl),
        );
        Navigator.of(context).push(router);
      } else if (type == "CUSTOM") {
        addHistory("M", widget.videoDetail!.id);
        var router1 = new MaterialPageRoute(
          builder: (BuildContext context) => MyCustomPlayer(
            url: videoURL,
            title: title,
            downloadStatus: 1,
            subtitles: subtitles,
          ),
        );
        Navigator.of(context).push(router1);
      }
    } else {
      log("You can't access this content!");
      Fluttertoast.showToast(
        msg: translate("You_cant_access_this_content_"),
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0,
      );
    }
  }

  var appconfig;
  @override
  void initState() {
    super.initState();
    _scrollController = new ScrollController();
    showAds();
    newSeasonIndex = 0;
    setState(() {
      cSeasonIndex = 0;
      seasonEpisodeData = null;
    });

    _tabController = TabController(vsync: this, length: 2, initialIndex: 0);
    commentList = widget.videoDetail!.comments!;
    appconfig = Provider.of<AppConfig>(context, listen: false).appModel;
  }

  @override
  Widget build(BuildContext context) {
    var moreLikeThis = Provider.of<MenuDataProvider>(context).menuCatMoviesList;
    return DefaultTabController(
      length: 2,
      child: scaffold(moreLikeThis),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
    commentsController.dispose();
    subCommentsController.dispose();
    _scrollController!.dispose();
  }
}
