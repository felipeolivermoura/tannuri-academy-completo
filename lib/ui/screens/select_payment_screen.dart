import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:tannuri/common/styles.dart';
import '/common/apipath.dart';
import '/common/global.dart';
import '/common/route_paths.dart';
import '/providers/app_config.dart';
import '/providers/payment_key_provider.dart';
import '/providers/user_profile_provider.dart';
import '/ui/gateways/paypal/PaypalPayment.dart';
import '/ui/screens/apply_coupon_screen.dart';
import 'package:provider/provider.dart';

List listPaymentGateways = [];
String couponMSG = '';
var validCoupon, percentOFF, amountOFF;
bool isCouponApplied = true;
var mFlag = 0;
String couponCode = '';
var genCoupon; // Useless

var afterDiscountAmount;
bool isStripeCoupon = false;

class SelectPaymentScreen extends StatefulWidget {
  SelectPaymentScreen(this.planIndex);

  final planIndex;

  @override
  _SelectPaymentScreenState createState() => _SelectPaymentScreenState();
}

class _SelectPaymentScreenState extends State<SelectPaymentScreen>
    with TickerProviderStateMixin, RouteAware {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  bool isDataAvailable = false;
  bool loading = true;

  PageController hPagerController = PageController(keepPage: true);
  PageController vPagerController = PageController(keepPage: true);
  double mWidth = 100.0;
  double mHeight = 100.0;

  @override
  void initState() {
    super.initState();

    setState(() {
      loading = true;
    });
    isCouponApplied = true;
    mFlag = 0;
    validCoupon = false;
    couponCode = '';
    isStripeCoupon = false;
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      PaymentKeyProvider paymentKeyProvider =
          Provider.of<PaymentKeyProvider>(context, listen: false);
      await paymentKeyProvider.fetchPaymentKeys();

      listPaymentGateways = [];
      var payPal = Provider.of<AppConfig>(context, listen: false)
          .appModel!
          .appConfig!
          .paypalPayment;

      if (payPal == 1 || "$payPal" == "1") {
        listPaymentGateways
            .add(PaymentGateInfo(title: 'paypalPayment', status: 1));
      }

      setState(() {
        loading = false;
      });
    });
  }

//  Apply coupon forward icon
  Widget applyCouponIcon() {
    return Expanded(
      flex: 1,
      child: Padding(
        padding: EdgeInsets.only(left: 0.0),
        child: Icon(
          Icons.keyboard_arrow_right,
        ),
      ),
    );
  }

//  Gift icon
  Widget giftIcon() {
    return Padding(
      padding: EdgeInsets.only(left: 10.0),
      child: Icon(
        Icons.card_giftcard,
        color: Color.fromRGBO(125, 183, 91, 1.0),
      ),
    );
  }

//  App bar material design
  Widget appbarMaterialDesign() {
    return Material(
      child: Container(
        height: 80.0,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            stops: [0.1, 0.5, 0.7],
            colors: [
              Color.fromARGB(255, 246, 35, 35),
              Color.fromARGB(255, 246, 70, 35),
              Color.fromARGB(255, 246, 91, 35),
            ],
          ),
        ),
      ),
    );
  }

//  Select payment text
  Widget selectPaymentText() {
    var logo =
        Provider.of<AppConfig>(context, listen: false).appModel!.config!.logo;

    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 20.0, top: 40.0),
        ),
        Expanded(
          flex: 1,
          child: Text(
            translate('Select_Payment'),
            style: TextStyle(
              fontSize: 18.0,
              fontWeight: FontWeight.w800,
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: Container(
            padding: EdgeInsets.only(left: 40.0, right: 20.0),
            child: Image.network('${APIData.logoImageUri}$logo'),
          ),
        )
      ],
    );
  }

//  Plan name and user name
  Widget planAndUserName(indexPer) {
    var planDetails = Provider.of<AppConfig>(context).planList;
    var name =
        Provider.of<UserProfileProvider>(context).userProfileModel!.user!.name!;
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 20.0),
          ),
          Expanded(
            flex: 2,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  "${planDetails[widget.planIndex].name}",
                  style: TextStyle(
                      color: primaryRed,
                      fontSize: 14.0,
                      fontWeight: FontWeight.w600),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 15.0),
                ),
                Text(
                  name,
                  style: TextStyle(
                    fontSize: 12.0,
                    height: 1.3,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

//  Minimum duration
  Widget minDuration(indexPer) {
    var planDetails = Provider.of<AppConfig>(context).planList;
    return Expanded(
      flex: 2,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(
            translate('Min_duration') +
                ' ${planDetails[indexPer].intervalCount} ' +
                translate('months_'),
            style: TextStyle(
              fontSize: 12.0,
              height: 1.3,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 10.0),
          ),
          Text(
            new DateFormat.yMMMd().format(new DateTime.now()),
            style: TextStyle(
              fontSize: 12.0,
              height: 1.5,
            ),
          ),
        ],
      ),
    );
  }

//  After applying coupon
  Widget couponProcessing(afterDiscountAmount, indexPer) {
    var planDetails = Provider.of<AppConfig>(context).planList;
    return Container(
      margin: EdgeInsets.fromLTRB(20.0, 10.0, 20, 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            children: <Widget>[
              discountText(),
              Expanded(
                flex: 1,
                child: validCoupon == true && percentOFF != null
                    ? Text(
                        percentOFF.toString() + " %",
                        style: TextStyle(
                          fontSize: 12.0,
                          height: 1.3,
                        ),
                      )
                    : amountOFF != null
                        ? Text(
                            amountOFF.toString() +
                                " ${planDetails[widget.planIndex].currency}",
                            style: TextStyle(
                              fontSize: 12.0,
                              height: 1.3,
                            ),
                          )
                        : Text(
                            "0 %",
                            style: TextStyle(
                              fontSize: 12.0,
                              height: 1.3,
                            ),
                          ),
              ),
            ],
          ),
          SizedBox(
            height: 10.0,
          ),
          Row(
            children: <Widget>[
              afterDiscountText(),
              Expanded(
                flex: 1,
                child: validCoupon == true
                    ? Text(
                        afterDiscountAmount.toString() +
                            " ${planDetails[widget.planIndex].currency}",
                        style: TextStyle(
                          fontSize: 12.0,
                          height: 1.3,
                        ),
                      )
                    : amountText(indexPer),
              ),
            ],
          )
        ],
      ),
    );
  }

//  Plan amount
  Widget planAmountText(indexPer, dailyAmountAp) {
    var planDetails = Provider.of<AppConfig>(context).planList;
    return Expanded(
      flex: 2,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Text(
              "${currency(planDetails[indexPer].currency)}".toUpperCase() +
                  " ${planDetails[widget.planIndex].amount}",
              style: TextStyle(
                fontSize: 22.0,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          SizedBox(
            height: 3.0,
          ),
          Container(
            child: Text(
              '( ${currency(planDetails[widget.planIndex].currency)} $dailyAmountAp' +
                  ' / ${planDetails[widget.planIndex].interval} )',
              style: TextStyle(
                fontSize: 10.0,
                letterSpacing: 0.8,
                height: 1.3,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
        ],
      ),
    );
  }

//  Discount percent
  Widget discountText() {
    return Expanded(
      flex: 5,
      child: Text(
        translate("Discount_"),
        style: TextStyle(
          fontSize: 12.0,
          height: 1.3,
        ),
      ),
    );
  }

//  Amount after discount
  Widget afterDiscountText() {
    return Expanded(
      flex: 5,
      child: Text(
        translate("After_Discount_Amount_"),
        style: TextStyle(
          fontSize: 12.0,
          height: 1.3,
        ),
      ),
    );
  }

  String currency(code) {
    var format = NumberFormat.simpleCurrency(
      name: code, //currencyCode
    );
    print("CURRENCY SYMBOL ${format.currencySymbol}"); // $
    print("CURRENCY NAME ${format.currencyName}"); // USD
    return "${format.currencySymbol}";
  }

//  Amount
  Widget amountText(indexPer) {
    var planDetails = Provider.of<AppConfig>(context).planList;
    return Text(
      "${planDetails[indexPer].amount}" +
          " ${currency(planDetails[indexPer].currency)}",
      style: TextStyle(
        fontSize: 12.0,
        height: 1.3,
      ),
    );
  }

  // Paypal Payment wallet
  Widget paypalPayment(indexPer) {
    var userDetails = Provider.of<UserProfileProvider>(context, listen: false)
        .userProfileModel;
    var appConfig = Provider.of<AppConfig>(context, listen: false).appModel;
    var planDetails = Provider.of<AppConfig>(context).planList;
    planDetails.sort((a, b) => a.amount!.compareTo(b.amount!));

    return Container(
      color: Colors.black,
      child: Column(
        children: [
          SizedBox(
            height: 30,
          ),
          ClipRRect(
            borderRadius: new BorderRadius.all(
              Radius.circular(20),
            ),
            child: InkWell(
              child: Container(
                padding: EdgeInsets.all(18),
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    stops: [0.1, 0.5, 0.7],
                    colors: [
                      Color.fromARGB(255, 246, 35, 35),
                      Color.fromARGB(255, 246, 70, 35),
                      Color.fromARGB(255, 246, 91, 35),
                    ],
                  ),
                ),
                child: Text(
                  "CONTINUE WITH PAYPAL",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                  ),
                ),
              ),
              onTap: () {
                if (couponCode == '') {
                  onPayWithPayPal(
                    appConfig,
                    userDetails,
                    indexPer,
                    null,
                    planDetails,
                  );
                } else {
                  if (afterDiscountAmount > 0 && !isStripeCoupon) {
                    onPayWithPayPal(
                      appConfig,
                      userDetails,
                      indexPer,
                      afterDiscountAmount,
                      planDetails,
                    );
                  } else {
                    onPayWithPayPal(
                      appConfig,
                      userDetails,
                      indexPer,
                      null,
                      planDetails,
                    );
                  }
                }
                return null;
              },
            ),
          ),
        ],
      ),
    );
  }

  void onPayWithPayPal(appConfig, userDetails, indexPer, amount, planDetails) {
    print("currency codes for pay pal : ${appConfig.config.currencyCode}");
    print("currency codes for amount1 : ${appConfig.plans[indexPer].amount}");
    print("currency codes for amount2 : $amount");
    print("id for paypal : ${appConfig.plans[widget.planIndex].id}");
    print("name for paypal : ${appConfig.plans[widget.planIndex].name}");

    print("id for paypal : ${planDetails[widget.planIndex].id}");
    print("name for paypal : ${planDetails[widget.planIndex].name}");

    Navigator.pushNamed(
      context,
      RoutePaths.paypal,
      arguments: PaypalPayment(
        onFinish: (number) async {},
        currency: "${planDetails[widget.planIndex].currency}",
        userFirstName: userDetails.user.name,
        userLastName: "",
        userEmail: userDetails.user.email,
        payAmount: amount == null
            ? "${planDetails[widget.planIndex].amount}"
            : "$amount",
        planName: planDetails[widget.planIndex].name,
        planIndex: planDetails[widget.planIndex].id,
      ),
    );
  }

  //  Sliver List
  Widget _sliverList(dailyAmountAp, afterDiscountAmount, planDetails) {
    return Container(
      child: Column(
        children: <Widget>[
          new Container(
            child: Column(
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    appbarMaterialDesign(),
                    Container(
                      margin: EdgeInsets.only(top: 60.0),
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColorLight,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20.0),
                            topRight: Radius.circular(20.0)),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          AspectRatio(
                            aspectRatio:
                                validCoupon == true ? 16.0 / 15.0 : 16.0 / 13.0,
                            child: Column(
                              children: <Widget>[
                                Padding(
                                  padding:
                                      EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
                                ),
                                selectPaymentText(),
                                planAndUserName(widget.planIndex),
                                Padding(
                                  padding:
                                      EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Padding(
                                        padding: EdgeInsets.only(left: 20.0),
                                      ),
                                      minDuration(widget.planIndex),
                                      planAmountText(
                                          widget.planIndex, dailyAmountAp),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 40.0),
                                ),
                                InkWell(
                                  child: Container(
                                    margin: EdgeInsets.only(
                                        left: 20.0, right: 20.0),
                                    height: 50.0,
                                    width: MediaQuery.of(context).size.width,
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Expanded(
                                          flex: 5,
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: <Widget>[
                                              giftIcon(),
                                              Padding(
                                                padding:
                                                    EdgeInsets.only(left: 10.0),
                                                child: isCouponApplied
                                                    ? Text("Apply Coupon")
                                                    : Text(
                                                        couponCode,
                                                        textAlign:
                                                            TextAlign.left,
                                                      ),
                                              )
                                            ],
                                          ),
                                        ),
                                        applyCouponIcon(),
                                      ],
                                    ),
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        width: 2.0,
                                      ),
                                    ),
                                  ),
                                  onTap: () {
                                    Navigator.pushNamed(
                                      context,
                                      RoutePaths.applyCoupon,
                                      arguments: ApplyCouponScreen(
                                        planDetails[widget.planIndex].amount,
                                        setState_,
                                      ),
                                    );
                                  },
                                ),
                                Container(
                                  height: 30.0,
                                  child: isCouponApplied
                                      ? SizedBox.shrink()
                                      : Padding(
                                          padding: EdgeInsets.only(left: 40.0),
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: <Widget>[
                                              validCoupon == true
                                                  ? Icon(
                                                      FontAwesomeIcons
                                                          .solidCircleCheck,
                                                      color: activeDotColor,
                                                      size: 13.0,
                                                    )
                                                  : Icon(
                                                      FontAwesomeIcons
                                                          .solidCircleXmark,
                                                      color: Colors.red,
                                                      size: 13.0,
                                                    ),
                                              SizedBox(
                                                width: 10.0,
                                              ),
                                              Text(
                                                couponMSG,
                                                style: TextStyle(
                                                  color: validCoupon == true
                                                      ? Colors.green
                                                      : Colors.red,
                                                  fontSize: 12.0,
                                                  letterSpacing: 0.7,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                ),
                                validCoupon == true
                                    ? couponProcessing(
                                        afterDiscountAmount,
                                        widget.planIndex,
                                      )
                                    : SizedBox.shrink(),
                              ],
                            ),
                          ),
                          Container(
                            height: 2.0,
                          ),
                        ],
                      ),
                    ),
                    new Positioned(
                      top: 8.0,
                      left: 4.0,
                      child: new BackButton(color: Colors.white),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  //  Scaffold body
  Widget _scaffoldBody(dailyAmountAp, afterDiscountAmount, planDetails) {
    return Container(
      color: Colors.black,
      height: MediaQuery.of(context).size.height,
      child: SingleChildScrollView(
        physics: NeverScrollableScrollPhysics(),
        child: Column(
          children: [
            _sliverList(dailyAmountAp, afterDiscountAmount, planDetails),
            _nestedScrollViewBody(),
          ],
        ),
      ),
    );
  }

  // NestedScrollView body
  Widget _nestedScrollViewBody() {
    return paypalPayment(widget.planIndex);
  }

  void setState_() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    var planDetails = Provider.of<AppConfig>(context).planList;
    var dailyAmount1;
    var intervalCount;
    dynamic planAm = planDetails[widget.planIndex].amount;
    switch (planAm.runtimeType) {
      case int:
        dailyAmount1 = planAm;
        break;
      case String:
        dailyAmount1 = double.parse(planAm);
        break;
      case double:
        dailyAmount1 = planAm;
        break;
    }
    dynamic interCount = planDetails[widget.planIndex].intervalCount;
    switch (interCount.runtimeType) {
      case int:
        intervalCount = interCount;
        break;
      case String:
        intervalCount = int.parse(interCount);
        break;
    }
    var dailyAmount = dailyAmount1 / intervalCount;
    String? dailyAmountAp = dailyAmount.toStringAsFixed(2);
    var planAmount;
    if (planDetails[widget.planIndex].amount != null) {
      if (planDetails[widget.planIndex].amount.runtimeType == String) {
        planAmount = double.parse(planDetails[widget.planIndex].amount);
      } else {
        planAmount = planDetails[widget.planIndex].amount;
      }
    }
    var amountOff = validCoupon == true
        ? percentOFF != null
            ? (percentOFF / 100) * planAmount
            : amountOFF
        : 0;
    afterDiscountAmount = validCoupon == true ? planAmount - amountOff : 0;

    return SafeArea(
      child: WillPopScope(
        child: DefaultTabController(
          length: 2,
          child: Scaffold(
            key: _scaffoldKey,
            body: loading == true
                ? Center(
                    child: CircularProgressIndicator(
                      valueColor: new AlwaysStoppedAnimation<Color>(
                          Theme.of(context).primaryColor),
                    ),
                  )
                : _scaffoldBody(
                    dailyAmountAp, afterDiscountAmount, planDetails),
          ),
        ),
        onWillPop: () async {
          return true;
        },
      ),
    );
  }
}

class PaymentGateInfo {
  String? title;
  dynamic status;

  PaymentGateInfo({this.title, this.status});
}
