import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:tannuri/models/AppUiShortingModel.dart';
import 'package:tannuri/providers/app_ui_shorting_provider.dart';
import 'package:tannuri/ui/widgets/horizontal_plans_list.dart';
import 'package:tannuri/ui/screens/recommended_video_list.dart';
import '/providers/menu_data_provider.dart';
import '/ui/shared/heading1.dart';
import '/ui/screens/horizental_genre_list.dart';
import '/ui/screens/horizontal_movies_list.dart';
import 'package:provider/provider.dart';
import 'home-screen-shimmer.dart';

class VideosPage extends StatefulWidget {
  VideosPage({Key? key, this.loading, this.menuId, this.menuSlug})
      : super(key: key);
  final loading;
  final menuId;
  final menuSlug;

  @override
  _VideosPageState createState() => _VideosPageState();
}

class _VideosPageState extends State<VideosPage>
    with AutomaticKeepAliveClientMixin, TickerProviderStateMixin {
  late AnimationController animation;
  late Animation<double> _fadeInFadeOut;

  @override
  bool get wantKeepAlive => true;

  GlobalKey _keyRed = GlobalKey();
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  var meData;
  ScrollController controller = ScrollController(initialScrollOffset: 0.0);
  bool _visible = false;
  var menuDataList;
  var moviesList;
  var topVideosList;
  var recommendedVideosList;
  var recommendedVideosListLen;
  var moviesListLen;

  MenuDataProvider menuDataProvider = MenuDataProvider();
  @override
  void initState() {
    super.initState();

    animation = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 1500),
    );
    _fadeInFadeOut = Tween<double>(begin: 0.2, end: 1.0).animate(animation);

    animation.forward();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      try {
        await Provider.of<AppUIShortingProvider>(context, listen: false)
            .loadData(context);

        menuDataProvider =
            Provider.of<MenuDataProvider>(context, listen: false);
        await menuDataProvider.getMenusData(context, widget.menuId);
        await menuDataProvider.getTopData(context, widget.menuSlug);
        await menuDataProvider.getRecommendedData(context, widget.menuSlug);

        moviesList = menuDataProvider.menuCatMoviesList;
        menuDataList = menuDataProvider.menuDataList;
        topVideosList = menuDataProvider.topMovieTVSeries;
        recommendedVideosList = menuDataProvider.recommendedMovieTVSeries;
        topVideosList = topVideosList..shuffle();
        recommendedVideosList = recommendedVideosList..shuffle();
        menuDataList = menuDataList..shuffle();
        moviesList = moviesList..shuffle();
        recommendedVideosListLen = recommendedVideosList.length;
        moviesListLen = moviesList.length;

        if (mounted) {
          setState(() {
            _visible = true;
          });
        }
      } catch (err) {
        print("Da Error $err");
        return null;
      }
    });
  }

  Future<Null> refreshList() async {
    await Future.delayed(Duration(seconds: 2));
    getMenuData();
  }

  getMenuData() async {
    try {
      await Provider.of<AppUIShortingProvider>(context, listen: false)
          .loadData(context);

      menuDataProvider = Provider.of<MenuDataProvider>(context, listen: false);
      await menuDataProvider.getMenusData(context, widget.menuId);
      await menuDataProvider.getTopData(context, widget.menuSlug);
      await menuDataProvider.getRecommendedData(context, widget.menuSlug);

      moviesList = menuDataProvider.menuCatMoviesList;
      menuDataList = menuDataProvider.menuDataList;
      topVideosList = menuDataProvider.topMovieTVSeries;
      recommendedVideosList = menuDataProvider.recommendedMovieTVSeries;
      moviesList.removeWhere((item) => item.live == '1' || item.live == 1);

      setState(() {
        topVideosList = topVideosList..shuffle();
        recommendedVideosList = recommendedVideosList..shuffle();
        menuDataList = menuDataList..shuffle();
        moviesList = moviesList..shuffle();
      });
    } catch (err) {
      return null;
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  List<Widget> widgetList() {
    List<Widget> widgets = [];

    AppUiShortingModel appUiShortingModel =
        Provider.of<AppUIShortingProvider>(context, listen: false)
            .appUiShortingModel;

    appUiShortingModel.appUiShorting?.forEach((element) {
      if (element.name == "genre") {
        widgets.add(
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(height: 20.0),
              HorizontalGenreList(
                loading: widget.loading,
              ),
              SizedBox(height: 15),
              // -----
              // -----
            ],
          ),
        );
      } else if (element.name == "movies") {
        widgets.add(
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              moviesListLen == 0
                  ? SizedBox.shrink()
                  : Heading1(translate("Movies_"), "Mov", widget.loading),
              moviesListLen == 0
                  ? SizedBox.shrink()
                  : MoviesList(
                      loading: widget.loading,
                      data: moviesList,
                    ),
              moviesListLen == 0 ? SizedBox.shrink() : SizedBox(height: 15.0),
            ],
          ),
        );
      } else if (element.name == "trending") {
        widgets.add(
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              recommendedVideosListLen == 0
                  ? SizedBox.shrink()
                  : Heading1(
                      translate("Recommended_"),
                      "Recommended",
                      widget.loading,
                      videoList: recommendedVideosList,
                    ),
              recommendedVideosListLen == 0
                  ? SizedBox.shrink()
                  : SizedBox(height: 15.0),
              recommendedVideosListLen == 0
                  ? SizedBox.shrink()
                  : Container(
                      height: 180,
                      child: RecommendedVideoList(
                        loading: widget.loading,
                        videoList: recommendedVideosList,
                      ),
                    ),
            ],
          ),
        );
      }
    });

    widgets.add(
      HorizontalPlansList(
        loading: false,
        bottom: 20,
      ),
    );

    widgets.add(
      Container(
        width: double.infinity,
        height: 220,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/background.png"),
            fit: BoxFit.contain,
          ),
        ),
      ),
    );

    return widgets;
  }

  @override
  // ignore: must_call_super
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: refreshList,
      color: Theme.of(context).primaryColor,
      backgroundColor: Theme.of(context).primaryColorLight,
      child: FadeTransition(
        opacity: _fadeInFadeOut,
        child: Container(
          child: _visible == false
              ? Center(
                  child: HomeScreenShimmer(
                    loading: true,
                  ),
                )
              : menuDataList.length == 0
                  ? Center(
                      child: Text(
                        translate("No_data_available"),
                        style: TextStyle(fontSize: 16.0),
                      ),
                    )
                  : Container(
                      child: SingleChildScrollView(
                        physics: ClampingScrollPhysics(),
                        child: Column(
                          key: _keyRed,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: widgetList(),
                        ),
                      ),
                    ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    animation.dispose();
    controller.dispose();
    super.dispose();
  }
}

void showDemoActionSheet(
    {required BuildContext context, required Widget child}) {
  showCupertinoModalPopup<String>(
      context: context,
      builder: (BuildContext context) => child).then((String? value) {});
}
