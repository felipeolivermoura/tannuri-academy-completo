import 'dart:math';
// ignore: import_of_legacy_library_into_null_safe
import 'package:currencies/currencies.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:intl/intl.dart';
import 'package:tannuri/ui/widgets/horizontal_plans_list.dart';
import '/common/styles.dart';
import '/models/plans_model.dart';
import '/providers/app_config.dart';
import '/providers/user_profile_provider.dart';
import 'package:provider/provider.dart';

class SubscriptionPlan extends StatefulWidget {
  @override
  _SubscriptionPlanState createState() => _SubscriptionPlanState();
}

class _SubscriptionPlanState extends State<SubscriptionPlan> {
  var bText = "First Trax";

  var buttonName;
  var buttonIndex;
  bool check = false;
  var selectedIndex = 0;
  var planDetails = [];
  var tableHeader = [];
  var plansFeatureDetails = [];
  var userDetails;
  var difference = 0;
  var strlen;
  bool pageLoading = true;

  static const int sortName = 0;
  bool isAscending = true;
  int sortType = sortName;
  var activePlanIndex;
  @override
  void initState() {
    super.initState();
    print("button : $buttonIndex");
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      plansData();
    });
  }

  Future plansData() async {
    var appConfigData = Provider.of<AppConfig>(context, listen: false);
    planDetails = appConfigData.planList;
    planDetails
        .removeWhere((element) => element.status == 0 || element.status == "0");
    planDetails.sort((a, b) => a.amount!.compareTo(b.amount!));
    tableHeader = List.from(planDetails);
    tableHeader.insert(0, Plan(name: ""));
    plansFeatureDetails = appConfigData.plansFeatures;

    print("plan features: ${plansFeatureDetails.length}");

    userDetails = Provider.of<UserProfileProvider>(context, listen: false)
        .userProfileModel!;
    setState(() {
      pageLoading = false;
    });
    if (userDetails.active == "1" || userDetails.active == 1) {
      print("Current Subscription : ${userDetails.currentSubscription}");
      difference = userDetails.end!.difference(userDetails.currentDate!).inDays;
      for (int index = 0; index < planDetails.length; index++) {
        print("Subscription ${index + 1} : ${planDetails[index].name}");
        if (userDetails.currentSubscriptionId == planDetails[index].id) {
          setState(() {
            selectedIndex = index;
            buttonName = translate("Already_Subscribed_with") +
                " ${planDetails[index].name}".toUpperCase();
            activePlanIndex = index;
          });
          break;
        } else {
          setState(() {
            selectedIndex = 0;
          });
        }
      }
    }
  }

  String generateRandomString(int len) {
    var r = Random();
    const _chars =
        'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
    var randomString;
    randomString =
        List.generate(len, (index) => _chars[r.nextInt(_chars.length)]).join();
    print(randomString);
    return randomString;
  }

  String currency(code) {
    code = "$code".toLowerCase();
    code = 'Iso4217Code.$code'.toString();
    var symbol;
    currencies.forEach((key, value) {
      if (code == "$key" && symbol == null) {
        symbol = value.symbol;
      }
    });
    if (symbol == null) {
      code = code.replaceAll("Iso4217Code.", "");
      code = code.toUpperCase();
      var format = NumberFormat.simpleCurrency(
        name: code, //currencyCode
      );

      print("Code: $code");
      print("CURRENCY SYMBOL ${format.currencySymbol}"); // $
      print("CURRENCY NAME ${format.currencyName}"); // USD
      return "${format.currencySymbol}";
    } else {
      return "$symbol";
    }
  }

  @override
  Widget build(BuildContext context) {
    var dW = MediaQuery.of(context).size.width;
    List.generate(plansFeatureDetails.length, (index) {
      print("str$index: ${plansFeatureDetails[index].name.length}");
      if (strlen == null || strlen < plansFeatureDetails[index].name.length) {
        setState(() {
          strlen = plansFeatureDetails[index].name.length;
        });
      }
    });
    print("strleng: $strlen");
    print("selected inde $selectedIndex");
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        centerTitle: true,
        title: Text(
          translate('Purchase_Memberships'),
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Theme.of(context).primaryColor,
            fontSize: 22.0,
          ),
        ),
      ),
      backgroundColor: Theme.of(context).primaryColorLight,
      body: pageLoading == true
          ? Center(
              child: CircularProgressIndicator(
              strokeWidth: 2.0,
              valueColor: AlwaysStoppedAnimation(primaryRed),
            ))
          : HorizontalPlansList(),
    );
  }
}
