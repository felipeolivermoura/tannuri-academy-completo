import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:tannuri/common/global.dart';
import 'package:tannuri/ui/screens/set_forgot_password_screen.dart';
import '/common/apipath.dart';
import '/common/route_paths.dart';
import '/common/styles.dart';
import '/providers/app_config.dart';
import '/ui/shared/logo.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  final TextEditingController _emailController = new TextEditingController();
  final TextEditingController _otpController = new TextEditingController();

  String msg = '';
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  bool _isLoading = false;

  bool hiddenOTP = true;
  bool hiddenEmail = false;

  @override
  void initState() {
    super.initState();

    setState(() {
      hiddenOTP = true;
      hiddenEmail = false;
    });
  }

  // Toggle for visibility
  void _otpVisibility() {
    setState(() {
      hiddenOTP = false;
      hiddenEmail = true;
    });
  }

  void _emailVisibility() {
    setState(() {
      hiddenOTP = true;
      hiddenEmail = false;
    });
  }

  // Send OTP code
  Future<String?> sendOtp() async {
    setState(() {
      _isLoading = true;
    });

    final sendOtpResponse = await http.post(APIData.forgotPasswordApi, body: {
      "email": _emailController.text,
    });
    print(sendOtpResponse.statusCode);
    print("otp: ${sendOtpResponse.body}");

    if (sendOtpResponse.statusCode == 200) {
      _otpVisibility();
    } else if (sendOtpResponse.statusCode == 401) {
      _emailVisibility();
      Fluttertoast.showToast(msg: translate("Email_address_doesnt_exist"));
    } else {
      Fluttertoast.showToast(msg: translate("Error_in_sending_OTP"));
      _emailVisibility();
    }

    setState(() {
      _isLoading = false;
    });

    return null;
  }

  // Verify OTP code
  Future<String?> verifyOtp() async {
    final sendOtpResponse = await http.post(APIData.verifyOTPApi, body: {
      "email": _emailController.text,
      "code": _otpController.text,
    });
    print("OTP res: ${sendOtpResponse.statusCode}");
    if (sendOtpResponse.statusCode == 200) {
      Navigator.pushNamed(
        context,
        RoutePaths.setForgotPassword,
        arguments: SetForgotPassword(
          (_emailController.text),
        ),
      );
    } else {
      Fluttertoast.showToast(msg: translate("Invalid_OTP"));
    }
    return null;
  }

  Widget emailField() {
    return Padding(
      padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 20.0),
      child: TextFormField(
        controller: _emailController,
        validator: (val) {
          if (val!.length == 0) {
            return translate('Email_cannot_be_empty');
          } else {
            if (!val.contains('@')) {
              return translate('Invalid_Email');
            } else {
              return null;
            }
          }
        },
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          hintText: translate("Enter_Email"),
          filled: true,
          fillColor: primaryRed.withOpacity(0.05),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(10.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(10.0),
          ),
          labelText: "E-mail",
          labelStyle: TextStyle(color: primaryRed),
        ),
        onSaved: (val) => _emailController.text = val!,
      ),
    );
  }

  Widget otpField() {
    return Padding(
      padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 20.0),
      child: TextFormField(
        controller: _otpController,
        validator: (val) {
          if (val!.length == 0) {
            return translate('Email_cannot_be_empty');
          } else {
            if (!val.contains('@')) {
              return translate('Invalid_Email');
            } else {
              return null;
            }
          }
        },
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          hintText: translate("Enter_OTP"),
          suffixIcon: IconButton(
            splashColor: Color.fromRGBO(72, 163, 198, 1.0),
            padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
            icon: Text(
              translate("Resend_"),
              style: TextStyle(color: activeDotColor, fontSize: 10),
            ),
            onPressed: () {
              sendOtp();
              Fluttertoast.showToast(msg: translate("OTP_Sent"));
            },
          ),
          filled: true,
          fillColor: primaryRed.withOpacity(0.05),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(10.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(10.0),
          ),
          labelText: "Code",
          labelStyle: TextStyle(color: primaryRed),
        ),
        onSaved: (val) => _otpController.text = val!,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    debugPaintSizeEnabled = false;
    var myModel = Provider.of<AppConfig>(context).appModel == null
        ? null
        : Provider.of<AppConfig>(context).appModel;

    return Shortcuts(
      shortcuts: <LogicalKeySet, Intent>{
        LogicalKeySet(LogicalKeyboardKey.select): ActivateIntent(),
      },
      child: Scaffold(
        backgroundColor: Theme.of(context).primaryColorDark,
        appBar: AppBar(
          leading: BackButton(
            onPressed: () => Navigator.pushNamed(context, RoutePaths.login),
          ),
          title: Text(
            "Forgot Password",
            style: TextStyle(
              fontSize: 16.0,
              letterSpacing: 0.9,
            ),
          ),
          centerTitle: true,
          backgroundColor: Theme.of(context).primaryColorDark,
        ),
        body: Column(
          children: [
            Flexible(
              flex: 1,
              child: Column(
                children: [
                  Flexible(
                    flex: 1,
                    child: logoImage(context, myModel, 0.9, 63.0, 200.0),
                  ),
                ],
              ),
            ),
            Flexible(
              flex: 4,
              child: Container(
                padding: EdgeInsets.only(top: 20.0),
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColorLight,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.0),
                    topRight: Radius.circular(20.0),
                  ),
                ),
                child: ListView(
                  children: [
                    Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          hiddenEmail ? SizedBox.shrink() : emailField(),
                          hiddenOTP ? SizedBox.shrink() : otpField(),
                          SizedBox(
                            height: 20,
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 15.0),
                            child: Row(
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                      ),
                                      backgroundColor: _isLoading == true
                                          ? Color.fromARGB(115, 111, 107, 107)
                                          : primaryRed,
                                      padding: EdgeInsets.symmetric(
                                        vertical: 15.0,
                                      ),
                                    ),
                                    child: _isLoading == true
                                        ? SizedBox(
                                            height: 20.0,
                                            width: 20.0,
                                            child: CircularProgressIndicator(
                                              strokeWidth: 2.0,
                                              valueColor:
                                                  AlwaysStoppedAnimation(
                                                primaryRed,
                                              ),
                                            ),
                                          )
                                        : Text(
                                            hiddenOTP == true
                                                ? translate("Send_OTP")
                                                : translate("Reset_Password"),
                                            style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.white,
                                            ),
                                          ),
                                    onPressed: () {
                                      if (hiddenOTP == true) {
                                        final form = _formKey.currentState!;
                                        form.save();
                                        if (form.validate() == true) {
                                          sendOtp();
                                          _emailVisibility();
                                        }
                                      } else {
                                        verifyOtp();
                                      }
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    Center(
                      child: _isLoading != true
                          ? InkWell(
                              child: new RichText(
                                text: new TextSpan(
                                  children: [
                                    new TextSpan(
                                      text: translate("To_back"),
                                      style: new TextStyle(
                                        color: isLight
                                            ? Colors.black54
                                            : Colors.white,
                                        fontSize: 16.5,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              onTap: () => Navigator.pushNamed(
                                  context, RoutePaths.login),
                            )
                          : null,
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class LwaButtonCustom extends StatefulWidget {
  final VoidCallback onPressed;

  const LwaButtonCustom({
    Key? key,
    required this.onPressed,
  }) : super(key: key);

  @override
  _LwaButtonCustomState createState() => _LwaButtonCustomState();
}

class _LwaButtonCustomState extends State<LwaButtonCustom> {
  static const String btnImageUnpressed =
      'assets/btnlwa_gold_loginwithamazon.png';
  static const String btnImagePressed =
      'assets/btnlwa_gold_loginwithamazon_pressed.png';
  String _btnImage = btnImageUnpressed;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: (tap) {
        setState(() {
          setState(() {
            _btnImage = btnImagePressed;
          });
        });
      },
      onTapUp: (tap) {
        setState(() {
          setState(() {
            _btnImage = btnImageUnpressed;
          });
        });
      },
      child: Container(
        child: IconButton(
          icon: Image(image: AssetImage(_btnImage)),
          iconSize: 40,
          onPressed: widget.onPressed,
        ),
      ),
    );
  }
}
