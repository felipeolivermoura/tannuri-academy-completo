import 'dart:io';
import 'dart:math';

import 'package:currencies/currencies.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tannuri/common/apipath.dart';
import 'package:tannuri/common/global.dart';
import 'package:tannuri/common/route_paths.dart';
import 'package:tannuri/common/styles.dart';
import 'package:tannuri/models/plans_model.dart';
import 'package:tannuri/providers/app_config.dart';
import 'package:tannuri/providers/user_profile_provider.dart';
import 'package:http/http.dart' as http;
import 'package:tannuri/ui/screens/select_payment_screen.dart';

bool? boolValue;
var checkConnectionStatus;
String? dailyAmountAp;
var dailyAmount;

class HomePlans extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var planDetails = Provider.of<AppConfig>(context).planList;

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: _buildCards(context, planDetails.length, planDetails),
        ),
      ),
    );
  }

  List<Widget> _buildCards(
      BuildContext context, int count, List<Plan> planDetails) {
    bool check = false;
    planDetails.sort((a, b) => a.amount!.compareTo(b.amount!));
    var userDetails = Provider.of<UserProfileProvider>(context, listen: false)
        .userProfileModel!;

    List<Widget> cards = List.generate(count, (int index) {
      var buttonName;
      if (userDetails.active == "1" || userDetails.active == 1) {
        final difference =
            userDetails.end!.difference(userDetails.currentDate!).inDays;

        print(' : ${planDetails[index].id}');
        if (userDetails.currentSubscription != null && difference >= 0) {
          if (userDetails.currentSubscriptionId == planDetails[index].id) {
            buttonName = translate("Already_Subscribed");
            check = true;
          } else {
            if (check == true) {
              buttonName = translate("Upgrade_");
            } else {
              buttonName = translate("Downgrade_");
            }
          }
        } else {
          buttonName = translate("Subscribe_");
        }
      } else {
        buttonName = translate("Subscribe_");
      }

      dynamic planAm = planDetails[index].amount;

      switch (planAm.runtimeType) {
        case int:
          {
            dailyAmount =
                planDetails[index].amount / planDetails[index].intervalCount;
            dailyAmountAp = dailyAmount.toStringAsFixed(2);
          }
          break;
        case String:
          {
            dailyAmount = double.parse(planDetails[index].amount.toString()) /
                double.parse(planDetails[index].intervalCount.toString());
            dailyAmountAp = dailyAmount.toStringAsFixed(2);
          }
          break;
        case double:
          {
            dailyAmount = double.parse(planDetails[index].amount.toString()) /
                double.parse(planDetails[index].intervalCount.toString());
            dailyAmountAp = dailyAmount.toStringAsFixed(2);
          }
          break;
      }

//      Used to check soft delete status so that only active plan can be showed
      dynamic mPlanStatus = planDetails[index].status;

      if (mPlanStatus.runtimeType == int) {
        if (planDetails[index].status == 1) {
          return planDetails[index].deleteStatus == 0
              ? SizedBox.shrink()
              : Container(
                  margin: EdgeInsets.only(top: 10.0),
                  child: subscriptionCards(
                      context, index, dailyAmountAp, buttonName),
                );
        } else {
          return SizedBox.shrink();
        }
      } else {
        if ("${planDetails[index].status}" == "active") {
          return "${planDetails[index].deleteStatus}" == "0"
              ? SizedBox.shrink()
              : subscriptionCards(context, index, dailyAmountAp, buttonName);
        } else {
          return SizedBox.shrink();
        }
      }
    });
    return cards;
  }

  String generateRandomString(int len) {
    var r = Random();
    const _chars =
        'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
    var randomString;
    randomString =
        List.generate(len, (index) => _chars[r.nextInt(_chars.length)]).join();
    print(randomString);
    return randomString;
  }

  Future freeSub(BuildContext context, dynamic id, double amount,
      String reference, dynamic status, String free) async {
    final freeSubscription =
        await http.post(Uri.parse(APIData.freeSubscription), headers: {
      HttpHeaders.authorizationHeader: "Bearer $authToken",
      "Accept": "application/json",
    }, body: {
      "plan_id": '$id',
      "amount": '$amount',
      "reference": '$reference',
      "status": '$status',
      "method": '$free',
    });
    print('refrence  $reference');
    if (freeSubscription.statusCode == 200) {
      print('Free Subscription Status Code : ${freeSubscription.statusCode}');
      Fluttertoast.showToast(
        msg: translate("Subscribed_Successfully"),
      );
      Navigator.pushNamed(context, RoutePaths.splashScreen);
    } else {
      print('Free Subscription Status Code : ${freeSubscription.statusCode}');
      Fluttertoast.showToast(
        msg: translate("Error_in_subscription"),
      );
    }
    return null;
  }

  Widget subscribeButton(BuildContext context, index, buttonName) {
    var planDetails = Provider.of<AppConfig>(context, listen: false).planList;
    return Padding(
      padding: EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Material(
            borderRadius: BorderRadius.circular(25.0),
            child: Container(
              height: 40.0,
              width: 150.0,
              decoration: BoxDecoration(
                borderRadius: new BorderRadius.circular(20.0),
                gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  stops: [0.1, 0.3, 0.5, 0.7, 1.0],
                  colors: [
                    buttonName == translate("Already_Subscribed")
                        ? activeDotColor
                        : primaryRed,
                    buttonName == translate("Already_Subscribed")
                        ? activeDotColor
                        : Color.fromRGBO(222, 29, 29, 0.906),
                    buttonName == translate("Already_Subscribed")
                        ? activeDotColor
                        : Color.fromRGBO(204, 32, 32, 0.808),
                    buttonName == translate("Already_Subscribed")
                        ? activeDotColor
                        : Color.fromRGBO(216, 36, 36, 0.71),
                    buttonName == translate("Already_Subscribed")
                        ? activeDotColor
                        : Color.fromRGBO(216, 32, 32, 0.612),
                  ],
                ),
              ),
              child: new MaterialButton(
                height: 50.0,
                splashColor: Color.fromRGBO(72, 163, 198, 0.9),
                child: Text(
                  buttonName,
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  print("free  ${planDetails[index].free}");
                  // Working after clicking on subscribe button
                  // if free subscription
                  if (planDetails[index].free == 1 ||
                      planDetails[index].free == "1") {
                    print("Plan ID ${planDetails[index].id}");
                    print("Amount ${planDetails[index].amount}");
                    print(
                        "free subscription status ${planDetails[index].status}");
                    print("free subscription ${planDetails[index].free}");

                    freeSub(context, planDetails[index].id, 0.00,
                        generateRandomString(8), 1, 'Free');
                  } else {
                    var router = new MaterialPageRoute(
                        builder: (BuildContext context) =>
                            new SelectPaymentScreen(index));
                    Navigator.of(context).push(router);
                  }
                },
              ),
            ),
          ),
          SizedBox(height: 8.0),
        ],
      ),
    );
  }

  String currency(code) {
    code = "$code".toLowerCase();
    code = 'Iso4217Code.$code'.toString();
    var symbol;
    currencies.forEach((key, value) {
      if (code == "$key" && symbol == null) {
        symbol = value.symbol;
      }
    });
    if (symbol == null) {
      code = code.replaceAll("Iso4217Code.", "");
      code = code.toUpperCase();
      var format = NumberFormat.simpleCurrency(
        name: code, //currencyCode
      );

      print("Code: $code");
      print("CURRENCY SYMBOL ${format.currencySymbol}"); // $
      print("CURRENCY NAME ${format.currencyName}"); // USD
      return "${format.currencySymbol}";
    } else {
      return "$symbol";
    }
  }

  //  Amount with currency
  Widget amountCurrencyText(BuildContext context, index) {
    var planDetails = Provider.of<AppConfig>(context, listen: false).planList;
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Expanded(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('${currency(planDetails[index].currency)}'),
              SizedBox(
                width: 3.0,
              ),
              Text(
                "${planDetails[index].amount}",
                style: TextStyle(
                  fontSize: 25.0,
                ),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ],
    );
  }

  //  Plan Name
  Widget planNameText(BuildContext context, index) {
    var planDetails = Provider.of<AppConfig>(context, listen: false).planList;
    print(planDetails[index].name);
    return Container(
      height: 35.0,
      color: primaryRed,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Text(
              '${planDetails[index].name}',
              style: TextStyle(color: Colors.white),
              textAlign: TextAlign.center,
            ),
          ),
        ],
      ),
    );
  }

  //  Subscription cards
  Widget subscriptionCards(
      BuildContext context, index, dailyAmountAp, buttonName) {
    return Card(
      clipBehavior: Clip.antiAlias,
      color: Theme.of(context).cardColor,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          AspectRatio(
            aspectRatio: 18.0 / 5.0,
            child: Column(
              children: <Widget>[
                planNameText(context, index),
                Padding(
                  padding: EdgeInsets.only(top: 10.0),
                ),
                amountCurrencyText(context, index)
              ],
            ),
          ),
          subscribeButton(context, index, buttonName),
        ],
      ),
    );
  }
}
