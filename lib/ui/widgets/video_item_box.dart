import 'package:flutter/material.dart';
import '/common/apipath.dart';
import '/models/datum.dart';

class VideoItemBox extends StatelessWidget {
  static const IMAGE_RATIO = 1.50;

  VideoItemBox(this.buildContext, this.videoDetail, {this.height = 120.0});
  final BuildContext buildContext;
  final Datum? videoDetail;
  final double height;

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.circular(8.0),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8.0),
        child: videoDetail!.thumbnail == null
            ? Container(
                height: 180,
                width: 110.0,
                color: Colors.white54,
              )
            : FadeInImage.assetNetwork(
                image: "${APIData.movieImageUri}${videoDetail!.thumbnail}",
                placeholder: "assets/placeholder_box.jpg",
                imageErrorBuilder: (context, error, stackTrace) {
                  return Container(
                    height: 180,
                    width: 110.0,
                    color: Colors.white54,
                  );
                },
                height: 180.0,
                width: 110.0,
                fit: BoxFit.cover,
              ),
      ),
    );
  }
}
