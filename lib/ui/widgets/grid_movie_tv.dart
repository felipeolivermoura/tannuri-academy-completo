// ignore: must_be_immutable
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import '/providers/menu_data_provider.dart';
import '/ui/shared/appbar.dart';
import '/ui/shared/grid_video_container.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class GridMovieTV extends StatelessWidget {
  var moviesList = [];
  var newList = [];
  var upcomingList = [];
  List<Widget> videoList = [];
  @override
  Widget build(BuildContext context) {
    moviesList = Provider.of<MenuDataProvider>(context).menuCatMoviesList;
    newList = new List.from(moviesList);
    upcomingList = List.from(newList
        .where((item) => item.isUpcoming == "1" || item.isUpcoming == 1));
    videoList.clear();
    videoList = List.generate(
      moviesList.length,
      (index) {
        return GridVideoContainer(context, moviesList[index]);
      },
    );

    return Scaffold(
      appBar: customAppBar(
        context,
        translate("Movies_"),
      ) as PreferredSizeWidget?,
      body: GridView.count(
        padding: EdgeInsets.only(
          left: 15.0,
          right: 15.0,
          top: 15.0,
          bottom: 15.0,
        ),
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        physics: ClampingScrollPhysics(),
        crossAxisCount: 3,
        childAspectRatio: 18 / 28,
        crossAxisSpacing: 10.0,
        mainAxisSpacing: 8.0,
        children: videoList,
      ),
    );
  }
}
