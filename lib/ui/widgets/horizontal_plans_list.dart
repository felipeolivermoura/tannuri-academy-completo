import 'dart:math';

import 'package:currencies/currencies.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tannuri/common/utils.dart';
import 'package:tannuri/providers/app_config.dart';
import 'package:tannuri/providers/user_profile_provider.dart';
import 'package:tannuri/ui/screens/select_payment_screen.dart';
import '/common/global.dart';

class HorizontalPlansList extends StatefulWidget {
  HorizontalPlansList({this.loading = false, this.top = 20, this.bottom = 0});
  final bool loading;
  final double top;
  final double bottom;

  @override
  _HorizontalPlansListState createState() => _HorizontalPlansListState();
}

class _HorizontalPlansListState extends State<HorizontalPlansList> {
  String currency(code) {
    code = "$code".toLowerCase();
    code = 'Iso4217Code.$code'.toString();

    var symbol;
    currencies.forEach((key, value) {
      if (code == "$key" && symbol == null) {
        symbol = value.symbol;
      }
    });
    if (symbol == null) {
      code = code.replaceAll("Iso4217Code.", "");
      code = code.toUpperCase();
      var format = NumberFormat.simpleCurrency(
        name: code, //currencyCode
      );

      print("Code: $code");
      print("CURRENCY SYMBOL ${format.currencySymbol}"); // $
      print("CURRENCY NAME ${format.currencyName}"); // USD
      return "${format.currencySymbol}";
    } else {
      return "$symbol";
    }
  }

  String generateRandomString(int len) {
    var r = Random();
    const _chars =
        'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
    var randomString;
    randomString =
        List.generate(len, (index) => _chars[r.nextInt(_chars.length)]).join();
    print(randomString);
    return randomString;
  }

  @override
  Widget build(BuildContext context) {
    var planDetails = Provider.of<AppConfig>(context).planList;
    planDetails.sort((a, b) => a.amount!.compareTo(b.amount!));
    var userDetails = Provider.of<UserProfileProvider>(context, listen: false)
        .userProfileModel!;

    if (widget.loading == true) {
      return LoadingWidget();
    } else {
      Size size = MediaQuery.of(context).size;

      return Column(
        children: [
          SizedBox(
            height: widget.top,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15.0),
            child: Text(
              "Take your Jiu-Jitsu to the next level! \nJoin us today with PayPal.",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 15.5),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            height: 550,
            child: ListView.builder(
              physics: BouncingScrollPhysics(),
              padding: EdgeInsets.symmetric(horizontal: 5),
              scrollDirection: Axis.horizontal,
              itemCount: planDetails.length,
              itemBuilder: (BuildContext context, int index) {
                var buttonName;
                if (userDetails.active == "1" || userDetails.active == 1) {
                  final difference = userDetails.end!
                      .difference(userDetails.currentDate!)
                      .inDays;

                  print(' : ${planDetails[index].id}');
                  if (userDetails.currentSubscription != null &&
                      difference >= 0) {
                    if (userDetails.currentSubscriptionId ==
                        planDetails[index].id) {
                      buttonName =
                          translate("Already_Subscribed").toUpperCase();
                    } else {
                      buttonName = translate("Subscribe_").toUpperCase();
                    }
                  } else {
                    buttonName = translate("Subscribe_").toUpperCase();
                  }
                } else {
                  buttonName = translate("Subscribe_").toUpperCase();
                }

                double amount = double.parse(planDetails[index].amount);

                if (int.parse("${planDetails[index].intervalCount}") > 1) {
                  amount =
                      amount / int.parse("${planDetails[index].intervalCount}");
                }

                return Container(
                  width: size.width - 80,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15.0),
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.white),
                        borderRadius: new BorderRadius.all(Radius.circular(20)),
                        image: DecorationImage(
                          image: AssetImage("assets/plan_background.png"),
                          fit: BoxFit.cover,
                        ),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          ClipRRect(
                            borderRadius: new BorderRadius.only(
                              topLeft: Radius.circular(20),
                              topRight: Radius.circular(20),
                            ),
                            child: Container(
                              width: size.width - 30,
                              height: 60,
                              decoration: BoxDecoration(
                                color: planDetails[index].intervalCount != 12
                                    ? Colors.grey[900]
                                    : Colors.red[900],
                                border: Border(
                                  bottom: BorderSide(
                                    width: 1,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              child: Text(
                                planDetails[index].name!,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  height: 2.5,
                                  color: Colors.white,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 3,
                            child: Container(
                              margin: EdgeInsets.only(top: 10),
                              width: 200,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Image.asset(
                                    "assets/plan_icon.png",
                                    height: Constants.genreListHeight,
                                    width: 50,
                                  ),
                                  Container(
                                    height: 2,
                                    width: 200,
                                    decoration: BoxDecoration(
                                      gradient: LinearGradient(
                                        colors: [
                                          Color(0xffa80000),
                                          Color(0xffd15400),
                                          Color(0xfff58300),
                                          Color(0xffdb8b00)
                                        ],
                                        stops: [0.25, 0.75, 0.87, 0.93],
                                        begin: Alignment.topLeft,
                                        end: Alignment.bottomRight,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    planDetails[index].free == 1
                                        ? "Access to the Begginers Course."
                                        : "Access to all content",
                                    style: TextStyle(
                                      fontSize: 18,
                                      color: Colors.white,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    planDetails[index].free == 1
                                        ? "All basic positions, drills and self defences"
                                        : "Classes divide by belts and techiques. All positions, drills, self defence and much more.",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 14, color: Colors.white54),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Container(
                                    width: 120,
                                    height: 120,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                          width: 2, color: Color(0xffd15400)),
                                      borderRadius: new BorderRadius.all(
                                        Radius.circular(60),
                                      ),
                                      color: Colors.black54,
                                    ),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              '${currency(planDetails[index].currency)}',
                                              style: TextStyle(
                                                fontSize: 20,
                                                color: Colors.white70,
                                              ),
                                            ),
                                            Text(
                                              amount.toString().split('.')[0],
                                              style: TextStyle(
                                                fontSize: 25,
                                                color: Colors.white70,
                                              ),
                                            ),
                                            Text(
                                              ",",
                                              style: TextStyle(
                                                fontSize: 18,
                                                color: Colors.white70,
                                              ),
                                            ),
                                            Text(
                                              amount
                                                  .toString()
                                                  .split('.')[1]
                                                  .padLeft(2, '0'),
                                              style: TextStyle(
                                                color: Colors.white70,
                                              ),
                                            ),
                                          ],
                                        ),
                                        if (planDetails[index].free != 1)
                                          Text(
                                            "per month",
                                            style: TextStyle(
                                              color: Colors.white70,
                                            ),
                                          ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                SizedBox(
                                  child: Text(
                                    planDetails[index].free == 1
                                        ? "32 BJJ tutorial videos"
                                        : "380 BJJ tutorial videos",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontSize: 14,
                                      color: Colors.white54,
                                    ),
                                  ),
                                ),
                                Container(
                                  width: 200,
                                  height: 40,
                                  child: InkWell(
                                    onTap: () {
                                      print("free  ${planDetails[index].free}");
                                      if (userDetails.currentSubscriptionId ==
                                          planDetails[index].id) {
                                        return;
                                      }
                                      // Working after clicking on subscribe button
                                      // if free subscription
                                      if (planDetails[index].free == 1 ||
                                          planDetails[index].free == "1") {
                                        print(
                                            "Plan ID ${planDetails[index].id}");
                                        print(
                                            "Amount ${planDetails[index].amount}");
                                        print(
                                            "free subscription status ${planDetails[index].status}");
                                        print(
                                            "free subscription ${planDetails[index].free}");

                                        freeSub(
                                            context,
                                            planDetails[index].id,
                                            0.00,
                                            generateRandomString(8),
                                            1,
                                            'Free');
                                      } else {
                                        var router = new MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                                new SelectPaymentScreen(index));
                                        Navigator.of(context).push(router);
                                      }
                                    },
                                    child: ClipRRect(
                                      borderRadius: new BorderRadius.all(
                                        Radius.circular(20),
                                      ),
                                      child: Container(
                                        decoration: BoxDecoration(
                                          gradient: LinearGradient(
                                            colors: [
                                              Color.fromARGB(255, 246, 70, 35),
                                              Color(0xffdb8b00)
                                            ],
                                            stops: [0.25, 0.75],
                                            begin: Alignment.topLeft,
                                            end: Alignment.bottomRight,
                                          ),
                                        ),
                                        child: Text(
                                          buttonName,
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            height: 2,
                                            color: Colors.white,
                                            fontSize: 16,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
          SizedBox(
            height: widget.bottom,
          ),
        ],
      );
    }
  }
}

class LoadingWidget extends StatelessWidget {
  const LoadingWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: Constants.genreListHeight,
      child: ListView.builder(
        padding: EdgeInsets.only(left: 15.0, right: 5.0),
        scrollDirection: Axis.horizontal,
        itemCount: 4,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            margin: EdgeInsets.only(right: Constants.genreItemRightMargin),
            width: Constants.genreItemWidth,
            height: Constants.genreItemHeight,
            child: Material(
              color: Colors.transparent,
              borderRadius: BorderRadius.circular(5.0),
              child: ClipRRect(
                borderRadius: new BorderRadius.circular(5.0),
                child: Image.asset(
                  "assets/placeholder_cover.jpg",
                  height: Constants.genreListHeight,
                  fit: BoxFit.fitWidth,
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
