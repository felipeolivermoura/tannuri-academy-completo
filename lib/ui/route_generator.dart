import 'package:flutter/material.dart';
import 'package:tannuri/localization/language_screen.dart';
import 'package:tannuri/ui/gateways/manual_payment_list.dart';
import 'package:tannuri/ui/screens/playlist_screen.dart';
import 'package:tannuri/ui/screens/set_forgot_password_screen.dart';
import 'package:tannuri/ui/shared/recommended_grid_view.dart';
import '/ui/screens/apply_coupon_screen.dart';
import '/ui/screens/change_password_screen.dart';
import '/ui/screens/forgot_password_screen.dart';
import '/ui/screens/manage_profile_screen.dart';
import '/ui/screens/notification_detail_screen.dart';
import '/ui/screens/update_profile_screen.dart';
import '../ui/gateways/paypal/PaypalPayment.dart';
import '../ui/screens/app_settings_screen.dart';
import '../ui/screens/donation_screen.dart';
import '../ui/screens/video_detail_screen.dart';
import 'package:page_transition/page_transition.dart';
import '../common/route_paths.dart';
import '../ui/screens/bottom_navigations_bar.dart';
import '../ui/screens/login_home.dart';
import '../ui/screens/login_screen.dart';
import '../ui/screens/membership_screen.dart';
import '../ui/screens/other_history_screen.dart';
import 'gateways/manual_payment_screen.dart';
import '../ui/screens/register_screen.dart';
import '../ui/screens/select_payment_screen.dart';
import '../ui/screens/subscription_plan_screen.dart';
import '../ui/screens/intro_screen.dart';
import 'screens/Downloaded_videos.dart';
import 'screens/home_screen.dart';
import 'screens/menu_screen.dart';
import 'screens/search_screen.dart';
import 'screens/splash_screen.dart';
import 'screens/video_grid_screen.dart';
import 'screens/wishlist_screen.dart';
import 'widgets/grid_movie_tv.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case RoutePaths.splashScreen:
        SplashScreen? argument = args as SplashScreen?;
        return MaterialPageRoute(
            builder: (context) => SplashScreen(
                  token: argument!.token,
                ));
      case RoutePaths.introSlider:
        return PageTransition(
            child: IntroScreen(), type: PageTransitionType.rightToLeft);
      case RoutePaths.loginHome:
        return PageTransition(
            child: LoginHome(), type: PageTransitionType.rightToLeft);
      case RoutePaths.login:
        return MaterialPageRoute(builder: (context) => LoginScreen());
      case RoutePaths.register:
        return MaterialPageRoute(builder: (context) => RegisterScreen());
      case RoutePaths.bottomNavigationHome:
        return MaterialPageRoute(builder: (_) => MyBottomNavigationBar());
      case RoutePaths.membership:
        return MaterialPageRoute(builder: (context) => MembershipScreen());
      case RoutePaths.donation:
        return PageTransition(
            child: DonationScreen(), type: PageTransitionType.rightToLeft);
      case RoutePaths.notificationDetail:
        NotificationDetailScreen argument = args as NotificationDetailScreen;
        return PageTransition(
            child: NotificationDetailScreen(argument.title, argument.message),
            type: PageTransitionType.rightToLeft);
      case RoutePaths.subscriptionPlans:
        return PageTransition(
            child: SubPlanScreen(), type: PageTransitionType.rightToLeft);
      case RoutePaths.applyCoupon:
        ApplyCouponScreen argument = args as ApplyCouponScreen;
        return PageTransition(
            child: ApplyCouponScreen(argument.amount, argument.func),
            type: PageTransitionType.rightToLeft);
      case RoutePaths.selectPayment:
        SelectPaymentScreen? argument = args as SelectPaymentScreen?;
        return MaterialPageRoute(
            builder: (context) => SelectPaymentScreen(argument!.planIndex));
      case RoutePaths.otherHistory:
        return MaterialPageRoute(builder: (context) => OtherHistoryScreen());
      case RoutePaths.manageProfile:
        return MaterialPageRoute(builder: (context) => ManageProfileScreen());
      case RoutePaths.changePassword:
        return MaterialPageRoute(builder: (context) => ChangePasswordScreen());
      case RoutePaths.updateProfile:
        return MaterialPageRoute(builder: (context) => UpdateProfileScreen());
      case RoutePaths.paypal:
        PaypalPayment? argument = args as PaypalPayment?;
        return MaterialPageRoute(
            builder: (context) => PaypalPayment(
                  onFinish: argument!.onFinish,
                  currency: argument.currency,
                  userFirstName: argument.userFirstName,
                  userLastName: argument.userLastName,
                  userEmail: argument.userEmail,
                  payAmount: argument.payAmount,
                  planIndex: argument.planIndex,
                ));
      case RoutePaths.setForgotPassword:
        SetForgotPassword? argument = args as SetForgotPassword?;
        return MaterialPageRoute(
            builder: (context) => SetForgotPassword(argument!.email));

      case RoutePaths.forgotPassword:
        return PageTransition(
            child: ForgotPassword(), type: PageTransitionType.rightToLeft);

      case RoutePaths.genreVideos:
        VideoGridScreen? argument = args as VideoGridScreen?;
        return MaterialPageRoute(
            builder: (context) => VideoGridScreen(
                argument!.id, argument.title, argument.genreDataList));
      case RoutePaths.gridVideos:
        return MaterialPageRoute(builder: (context) => GridMovieTV());
      case RoutePaths.videoDetail:
        VideoDetailScreen? argument = args as VideoDetailScreen?;
        return MaterialPageRoute(
            builder: (context) => VideoDetailScreen(argument!.videoDetail));
      case RoutePaths.appSettings:
        return PageTransition(
            child: AppSettingsScreen(), type: PageTransitionType.rightToLeft);
      case RoutePaths.mainHome:
        return MaterialPageRoute(builder: (context) => HomeScreen());
      case RoutePaths.search:
        return MaterialPageRoute(builder: (context) => SearchScreen());
      case RoutePaths.listPlaylists:
        return MaterialPageRoute(builder: (context) => PlaylistScreen());
      case RoutePaths.wishlist:
        WishListScreen? argument = args as WishListScreen?;
        return MaterialPageRoute(
            builder: (context) =>
                WishListScreen(argument!.playlistId, argument.playlistName));
      case RoutePaths.download:
        return MaterialPageRoute(builder: (context) => DownloadedVideos());
      case RoutePaths.menu:
        return MaterialPageRoute(builder: (context) => MenuScreen());
      case RoutePaths.ManualPaymentList:
        ManualPaymentList? argument = args as ManualPaymentList?;
        return MaterialPageRoute(
          builder: (context) => ManualPaymentList(
            manualPaymentModel: argument!.manualPaymentModel,
            planIndex: argument.planIndex,
            payAmount: argument.payAmount,
          ),
        );
      case RoutePaths.ManualPayment:
        ManualPaymentScreen? argument = args as ManualPaymentScreen?;
        return MaterialPageRoute(
          builder: (context) => ManualPaymentScreen(
            manualPayment: argument!.manualPayment,
            planIndex: argument.planIndex,
            payAmount: argument.payAmount,
          ),
        );
      case RoutePaths.ChooseLanguage:
        return MaterialPageRoute(
          builder: (context) => LanguageScreen(),
        );
      case RoutePaths.recommendedVideos:
        RecommendedGridView argument = args as RecommendedGridView;
        return MaterialPageRoute(
            builder: (context) => RecommendedGridView(argument.videoList));
      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Center(
              child: Text('No route defined for ${settings.name}'),
            ),
          ),
        );
    }
  }
}
