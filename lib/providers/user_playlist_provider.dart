import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:tannuri/models/user_playlist_model.dart';
import '/common/apipath.dart';
import '/common/global.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';

class UserPlaylistProvider with ChangeNotifier {
  UserPlaylistModel? userPlaylistModel;

  Future<UserPlaylistModel?> getUserPlaylist(BuildContext context) async {
    final response =
        await http.get(Uri.parse("${APIData.userPlaylistsApi}"), headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      HttpHeaders.authorizationHeader: "Bearer $authToken",
    });

    userPlaylistModel = UserPlaylistModel.fromJson(json.decode(response.body));
    notifyListeners();
    return userPlaylistModel;
  }
}
