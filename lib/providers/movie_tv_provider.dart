import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '../services/countryProvider.dart';
import '/common/route_paths.dart';
import '/providers/main_data_provider.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../common/apipath.dart';
import 'package:http/http.dart' as http;
import '../common/global.dart';
import '../models/comment.dart';
import '../models/datum.dart';
import '../models/movie_tv.dart';

class MovieTVProvider with ChangeNotifier {
  MovieTv? movieTv;
  List<Datum> movieTvList = [];
  List<Datum> moviesList = [];
  List<Datum> topVideoList = [];

  Future<MovieTv?> getMoviesTVData(BuildContext context) async {
    var genreList = Provider.of<MainProvider>(context, listen: false).genreList;
    try {
      var token;
      if (kIsWeb) {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        token = prefs.getString('token');
      } else {
        token = await storage.read(key: "authToken");
      }

      Locale myLocale = Localizations.localeOf(context);
      final response = await http
          .get(Uri.parse("${APIData.allMovies}&lang=${myLocale}"), headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        HttpHeaders.authorizationHeader: "Bearer $token",
      });
      if (response.statusCode == 200) {
        log('Movies :-> ${response.body}');
        log("Bearer Token :-> $token");
        movieTv = MovieTv.fromJson(json.decode(response.body));
        movieTv?.data?.removeWhere((element) =>
            element.country?.contains(countryName.toUpperCase()) == true);

        fetchMovieTVSeriesList(movieTv, genreList);

        topVideoList.removeWhere((element) =>
            element.status == 0 ||
            "${element.status}" == "0" ||
            element.country?.contains(countryName.toUpperCase()) == true);
        if (isKidsModeEnabled) {
          topVideoList.removeWhere((element) => element.isKids == 0);
        }
      } else {
        await storage.deleteAll();
        Navigator.pushNamed(context, RoutePaths.login);
        throw "Can't get movies and tv series";
      }
      notifyListeners();
    } catch (error) {
      print("Error :-> $error");
      await storage.deleteAll();
      Navigator.pushNamed(context, RoutePaths.login);
      throw error;
    }
    return movieTv;
  }

  fetchMovieTVSeriesList(movieTv, genreList) {
    movieTvList = List.generate(movieTv.data.length, (index) {
      var genreData = movieTv.data[index].genreId == null
          ? null
          : movieTv.data[index].genreId.split(",").toList();
      var subtitles = movieTv.data[index].subtitles == null
          ? null
          : movieTv.data[index].subtitles;
      return Datum(
        isKids: movieTv.data[index].isKids,
        id: movieTv.data[index].id,
        actorId: movieTv.data[index].actorId,
        title: movieTv.data[index].title,
        status: movieTv.data[index].status,
        keyword: movieTv.data[index].keyword,
        description: movieTv.data[index].description,
        duration: movieTv.data[index].duration,
        thumbnail: movieTv.data[index].thumbnail,
        poster: movieTv.data[index].poster,
        directorId: movieTv.data[index].directorId,
        detail: movieTv.data[index].detail,
        rating: movieTv.data[index].rating,
        maturityRating: movieTv.data[index].maturityRating,
        subtitle: movieTv.data[index].subtitle,
        subtitles: subtitles,
        publishYear: movieTv.data[index].publishYear,
        released: movieTv.data[index].released,
        uploadVideo: movieTv.data[index].uploadVideo,
        createdBy: movieTv.data[index].createdBy,
        createdAt: movieTv.data[index].createdAt,
        updatedAt: movieTv.data[index].updatedAt,
        isUpcoming: movieTv.data[index].isUpcoming,
        userRating: movieTv.data[index].userRating,
        videoLink: movieTv.data[index].videoLink,
        country: movieTv.data[index].country,
        genre: List.generate(genreData == null ? 0 : genreData.length,
            (int genreIndex) {
          return "${genreData[genreIndex]}";
        }),
        genres: List.generate(genreList.length, (int gIndex) {
          var genreId2 = genreList[gIndex].id.toString();
          var genreNameList = List.generate(
              genreData == null ? 0 : genreData.length, (int nameIndex) {
            return "${genreData[nameIndex]}";
          });
          var isAv2 = 0;
          for (var y in genreNameList) {
            if (genreId2 == y) {
              isAv2 = 1;
              break;
            }
          }
          if (isAv2 == 1) {
            if (genreList[gIndex].name == null) {
              return null;
            } else {
              return "${genreList[gIndex].name}";
            }
          }
          return null;
        }),
        comments: List.generate(
            movieTv.data[index].comments == null
                ? 0
                : movieTv.data[index].comments.length, (cIndex) {
          return Comment(
            id: movieTv.data[index].comments[cIndex].id,
            name: movieTv.data[index].comments[cIndex].name,
            email: movieTv.data[index].comments[cIndex].email,
            movieId: movieTv.data[index].comments[cIndex].movieId,
            comment: movieTv.data[index].comments[cIndex].comment,
            createdAt: movieTv.data[index].comments[cIndex].createdAt,
            updatedAt: movieTv.data[index].comments[cIndex].updatedAt,
          );
        }),
        episodeRuntime: movieTv.data[index].episodeRuntime,
        genreId: movieTv.data[index].genreId,
      );
    });
    movieTvList.removeWhere((element) =>
        element.status == 0 ||
        "${element.status}" == "0" ||
        element.country?.contains(countryName.toUpperCase()) == true);
    if (isKidsModeEnabled) {
      movieTvList.removeWhere((element) => element.isKids == 0);
    }
  }

  @override
  void dispose() {
    super.dispose();
  }
}
