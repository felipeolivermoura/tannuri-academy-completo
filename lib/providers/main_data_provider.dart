import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import '/common/apipath.dart';
import '/common/global.dart';
import '/common/route_paths.dart';
import '/models/genre_model.dart';

class MainProvider with ChangeNotifier {
  GenreModel? genreModel;
  List<Genre> genreList = [];

  Future<GenreModel?> getMainApiData(BuildContext context) async {
    Locale myLocale = Localizations.localeOf(context);

    try {
      final response = await http
          .get(Uri.parse('${APIData.allDataApi}&lang=${myLocale}'), headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        HttpHeaders.authorizationHeader: "Bearer $authToken",
      });
      print("Main API Status Code : ${response.statusCode}");
      log("Main API Response : ${response.body}");
      if (response.statusCode == 200) {
        genreModel = GenreModel.fromJson(json.decode(response.body));
        genreList = List.generate(genreModel!.genre!.length, (index) {
          return Genre(
            id: genreModel!.genre![index].id,
            name: genreModel!.genre![index].name,
            image: genreModel!.genre![index].image,
            createdAt: genreModel!.genre![index].createdAt,
            updatedAt: genreModel!.genre![index].updatedAt,
          );
        });
      } else {
        await storage.deleteAll();
        Navigator.pushNamed(context, RoutePaths.login);
        throw "Can't get main API data";
      }
      notifyListeners();
      return genreModel;
    } catch (error) {
      print(error);
      await storage.deleteAll();
      Navigator.pushNamed(context, RoutePaths.login);
      throw error;
    }
  }
}
