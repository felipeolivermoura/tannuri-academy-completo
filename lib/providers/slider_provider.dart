import 'dart:convert';
import 'dart:io';
import 'package:flutter/widgets.dart';
import '/common/apipath.dart';
import '../common/global.dart';
import '../models/slider_model.dart';
import 'package:http/http.dart' as http;

class SliderProvider with ChangeNotifier {
  SliderModel? sliderModel;

  Future<SliderModel?> getSlider(BuildContext context) async {
    Locale myLocale = Localizations.localeOf(context);

    final response = await http
        .get(Uri.parse("${APIData.sliderApi}&lang=${myLocale}"), headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      HttpHeaders.authorizationHeader: "Bearer $authToken",
    });
    sliderModel = SliderModel.fromJson(json.decode(response.body));
    notifyListeners();
    return sliderModel;
  }
}
