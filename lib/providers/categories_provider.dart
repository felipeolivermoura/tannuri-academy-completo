import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:tannuri/models/category_filter_model.dart';
import '/common/apipath.dart';
import '/common/global.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';

class CategoryProvider with ChangeNotifier {
  CategoryFilterModel? categoryModel;

  Future<CategoryFilterModel?> getCategories(
      BuildContext context, String genreId) async {
    Locale myLocale = Localizations.localeOf(context);
    final response = await http.get(
        Uri.parse(
            "${APIData.categoriesApi}/$genreId?secret=${APIData.secretKey}&lang=${myLocale}"),
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          HttpHeaders.authorizationHeader: "Bearer $authToken",
        });

    categoryModel = CategoryFilterModel.fromJson(json.decode(response.body));
    notifyListeners();
    return categoryModel;
  }
}
