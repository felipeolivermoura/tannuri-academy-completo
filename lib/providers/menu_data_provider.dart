import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:tannuri/services/countryProvider.dart';
import '../models/RecommendedDataModel.dart';
import '../models/TopDataModel.dart';
import '/common/apipath.dart';
import '/common/global.dart';
import 'package:http/http.dart' as http;
import '/models/datum.dart';
import '/models/menu_by_category.dart';
import '/providers/main_data_provider.dart';
import 'package:provider/provider.dart';
import 'movie_tv_provider.dart';

class MenuDataProvider with ChangeNotifier {
  MenuByCategory menuByCategory = new MenuByCategory();
  List<Datum> menuCatMoviesList = [];
  List<Datum> menuDataList = [];

  Future<MenuByCategory> getMenusData(BuildContext context, menuId) async {
    Locale myLocale = Localizations.localeOf(context);

    var genreList = Provider.of<MainProvider>(context, listen: false).genreList;

    menuDataList = [];
    menuCatMoviesList = [];
    final response = await http.get(
        Uri.parse(APIData.menuDataApi +
            "/$menuId?secret=${APIData.secretKey}&lang=${myLocale}"),
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          HttpHeaders.authorizationHeader: "Bearer $authToken",
        });
    print('Menu Data API Status Code ${response.statusCode}');
    log("Menu Data API Response -> ${response.body}");
    if (response.statusCode == 200) {
      menuByCategory = MenuByCategory.fromJson(json.decode(response.body));
      if (menuByCategory.data != null) {
        for (int i = 0; i < menuByCategory.data!.length; i++) {
          for (int j = 0; j < menuByCategory.data![i].length; j++) {
            var genreData = menuByCategory.data![i][j].genreId == null
                ? null
                : menuByCategory.data![i][j].genreId!.split(",").toList();

            menuDataList.add(Datum(
              isKids: menuByCategory.data![i][j].isKids,
              id: menuByCategory.data![i][j].id,
              tmdbId: menuByCategory.data![i][j].tmdbId,
              title: menuByCategory.data![i][j].title,
              keyword: menuByCategory.data![i][j].keyword,
              description: menuByCategory.data![i][j].description,
              duration: menuByCategory.data![i][j].duration,
              thumbnail: menuByCategory.data![i][j].thumbnail,
              poster: menuByCategory.data![i][j].poster,
              fetchBy: menuByCategory.data![i][j].fetchBy,
              directorId: menuByCategory.data![i][j].directorId,
              actorId: menuByCategory.data![i][j].actorId,
              genreId: menuByCategory.data![i][j].genreId,
              trailerUrl: menuByCategory.data![i][j].trailerUrl,
              detail: menuByCategory.data![i][j].detail,
              rating: menuByCategory.data![i][j].rating,
              maturityRating: menuByCategory.data![i][j].maturityRating,
              subtitle: menuByCategory.data![i][j].subtitle,
              subtitles: menuByCategory.data![i][j].subtitles,
              publishYear: menuByCategory.data![i][j].publishYear,
              released: menuByCategory.data![i][j].released,
              uploadVideo: menuByCategory.data![i][j].updatedAt,
              aLanguage: menuByCategory.data![i][j].aLanguage,
              audioFiles: menuByCategory.data![i][j].audioFiles,
              status: menuByCategory.data![i][j].status,
              createdBy: menuByCategory.data![i][j].createdBy,
              createdAt: menuByCategory.data![i][j].createdAt,
              isUpcoming: menuByCategory.data![i][j].isUpcoming,
              updatedAt: menuByCategory.data![i][j].updatedAt,
              userRating: menuByCategory.data![i][j].userRating,
              movieSeries: menuByCategory.data![i][j].movieSeries,
              videoLink: menuByCategory.data![i][j].videoLink,
              comments: menuByCategory.data![i][j].comments,
              episodeRuntime: menuByCategory.data![i][j].episodeRuntime,
              country: menuByCategory.data![i][j].country,
              genre: List.generate(genreData == null ? 0 : genreData.length,
                  (int genreIndex) {
                return "${genreData![genreIndex]}";
              }),
              genres: List.generate(genreList.length, (int gIndex) {
                var genreId2 = genreList[gIndex].id.toString();
                var genreNameList = List.generate(
                    genreData == null ? 0 : genreData.length, (int nameIndex) {
                  return "${genreData![nameIndex]}";
                });
                var isAv2 = 0;
                for (var y in genreNameList) {
                  if (genreId2 == y) {
                    isAv2 = 1;
                    break;
                  }
                }
                if (isAv2 == 1) {
                  if (genreList[gIndex].name == null) {
                    return null;
                  } else {
                    return "${genreList[gIndex].name}";
                  }
                }
                return null;
              }),
            ));

            menuDataList.removeWhere((element) =>
                element.status == 0 ||
                element.status == "0" ||
                element.country?.contains(countryName.toUpperCase()) == true);
            if (isKidsModeEnabled) {
              menuDataList.removeWhere((element) => element.isKids == 0);
            }
          }
        }
        for (int i = 0; i < menuDataList.length; i++) {
          menuCatMoviesList.add(Datum(
            isKids: menuDataList[i].isKids,
            id: menuDataList[i].id,
            actorId: menuDataList[i].actorId,
            title: menuDataList[i].title,
            trailerUrl: menuDataList[i].trailerUrl,
            status: menuDataList[i].status,
            keyword: menuDataList[i].keyword,
            description: menuDataList[i].description,
            duration: menuDataList[i].duration,
            thumbnail: menuDataList[i].thumbnail,
            poster: menuDataList[i].poster,
            directorId: menuDataList[i].directorId,
            detail: menuDataList[i].detail,
            rating: menuDataList[i].rating,
            maturityRating: menuDataList[i].maturityRating,
            subtitle: menuDataList[i].subtitle,
            subtitles: menuDataList[i].subtitles,
            publishYear: menuDataList[i].publishYear,
            released: menuDataList[i].released,
            uploadVideo: menuDataList[i].uploadVideo,
            aLanguage: menuDataList[i].aLanguage,
            createdBy: menuDataList[i].createdBy,
            createdAt: menuDataList[i].createdAt,
            updatedAt: menuDataList[i].updatedAt,
            isUpcoming: menuDataList[i].isUpcoming,
            userRating: menuDataList[i].userRating,
            movieSeries: menuDataList[i].movieSeries,
            videoLink: menuDataList[i].videoLink,
            comments: menuDataList[i].comments,
            episodeRuntime: menuDataList[i].episodeRuntime,
            genreId: menuDataList[i].genreId,
            tmdbId: menuDataList[i].tmdbId,
            fetchBy: menuDataList[i].fetchBy,
            genre: menuDataList[i].genre,
            genres: menuDataList[i].genres,
            country: menuDataList[i].country,
          ));

          menuCatMoviesList.removeWhere((element) =>
              element.status == 0 ||
              element.status == "0" ||
              element.country?.contains(countryName.toUpperCase()) == true);
          if (isKidsModeEnabled) {
            menuCatMoviesList.removeWhere((element) => element.isKids == 0);
          }
        }

        // Remove Duplicate Items

        List<Datum> _menuCatMoviesList = [];

        menuCatMoviesList.forEach((element) {
          if (_menuCatMoviesList.length > 0) {
            bool isAvailable = false;
            isAvailable = _menuCatMoviesList.any((_element) =>
                (element.id == _element.id && element.title == _element.title));
            if (!isAvailable) {
              _menuCatMoviesList.add(element);
            }
          } else {
            _menuCatMoviesList.add(element);
          }
        });

        menuCatMoviesList = _menuCatMoviesList;
      }
    } else {
      throw "Can't get menus data";
    }
    notifyListeners();
    return menuByCategory;
  }

  TopDataModel? topDataModel;
  List<Datum> topMovieTVSeries = [];

  Future<void> getTopData(BuildContext context, String? menuSlug) async {
    topMovieTVSeries = [];
    Locale myLocale = Localizations.localeOf(context);

    http.Response response = await http.get(
        Uri.parse(APIData.topData +
            "/$menuSlug?secret=${APIData.secretKey}&lang=${myLocale}"),
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          HttpHeaders.authorizationHeader: "Bearer $authToken",
        });
    print('Top Data API Status Code ${response.statusCode}');
    log("Top Data API Response -> ${response.body}");
    if (response.statusCode == 200) {
      topDataModel = topDataModelFromJson(response.body);

      topDataModel?.topData?.menuData?.forEach((_menuData) {
        if (_menuData.tvseries?.id == null) {
          Provider.of<MovieTVProvider>(context, listen: false)
              .moviesList
              .forEach((_movie) {
            if (_menuData.movie?.id.toString() == _movie.id.toString()) {
              topMovieTVSeries.add(_movie);
            }
          });
        }
      });
    }
  }

  RecommendedDataModel? recommendedDataModel;
  List<Datum> recommendedMovieTVSeries = [];

  Future<void> getRecommendedData(
      BuildContext context, String? menuSlug) async {
    Locale myLocale = Localizations.localeOf(context);
    recommendedMovieTVSeries = [];

    http.Response response = await http.get(
      Uri.parse("${APIData.recommendedData}&lang=${myLocale}"),
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        HttpHeaders.authorizationHeader: "Bearer $authToken",
      },
    );
    print('Recommended Data API Status Code ${response.statusCode}');
    log("Recommended Data API Responsed -> ${response.body}");
    if (response.statusCode == 200) {
      recommendedDataModel = recommendedDataModelFromJson(response.body);

      recommendedDataModel?.recomended?.forEach((_menuData) {
        if (_menuData.type == "M") {
          Provider.of<MovieTVProvider>(context, listen: false)
              .moviesList
              .forEach((_movie) {
            if (_menuData.id.toString() == _movie.id.toString()) {
              recommendedMovieTVSeries.add(_movie);
            }
          });
        }
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
  }
}
